FROM ruby:3.3
LABEL AUTHOR="Samuel Garratt"
LABEL url="https://gitlab.com/samuel-garratt/soaspec/blob/master/Dockerfile"
LABEL description='Dockerfile with soaspec gems preinstalled'

RUN mkdir /app
WORKDIR /app
COPY . /app
RUN gem install bundler
RUN bundle install

ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8

RUN groupadd -r test && useradd --no-log-init -r -g test test
USER test

ENTRYPOINT ["ruby", "exe/soaspec"]
CMD ["exec", "help"]
