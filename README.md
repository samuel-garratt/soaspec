# Soaspec

This gem helps to represent multiple API tests against a backend briefly, concisely and clearly.

![Soaspec example](images/basic_demo.gif)
> Example showing how a REST 'get' call can be made and it's response extracted.

>[![Build Status](https://gitlab.com/samuel-garratt/soaspec/badges/master/pipeline.svg)](https://gitlab.com/samuel-garratt/soaspec/pipelines)
[![Coverage](https://gitlab.com/samuel-garratt/soaspec/badges/master/coverage.svg)](https://samuel-garratt.gitlab.io/soaspec/coverage)

It is essentially a wrapper around the Savon and RestClient gems, adding useful functionality including

* Creating multiple API calls from the same base configuration through the use of an `ExchangeHandler` class
* Extracting values from response body's through either `XPath` or `JSONPath`
* Building up a custom RSpec `success scenario` shared example to reuse common tests on an API
* Methods simplifying setting and extracting values from a `Request/Response` pair (`Exchange`)
* Waiting for a particular response from an API by polling it
* Representing paths to values from a response with business-meaningful method names
* Generating initial code for testing an API with `soaspec new`
* Accessing and utilising `oauth2` access tokens
* Hosting a `virtual_server` that simulates REST & SOAP responses from an API

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'soaspec'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install soaspec

[Things to be done](Todo.md)

## Getting Started
### Git book
A book is in progress that will walk through creating an API test in Ruby and
how you can manage different variations of API tests in Soaspec. You can see
the draft [here](https://samuel-garratt.gitlab.io/soaspec_book/)

### Create a new test suite using RSpec
To create a new test suite using gem you can use the `soaspec` binary/executable.

Example:

```
mkdir 'api_test'
cd 'api_test'
soaspec new [rest/soap]
bundle install
```

This creates some RSpec tests for a sample SOAP/REST service. 
It creates tests that can be run through the `spec` Rake task. To understand
what the tests are doing, please read the [usage section](#usage).

```
rake spec
```

### Create an ExchangeHandler representing an API
To create a new handler from the command line use `soaspec add`.
E.g to generate handler for MyApi
```
soaspec add rest MyApi
```

`soaspec generate` can be used to help generate an exchange handler. This
will open a browser from which you can enter parameters and have an `ExchangeHandler` 
generated with the parameters entered.

### Generate test suite from a WSDL
A test suite can also be generated using a WSDL if a `--wsdl` option is passed to
the `soaspec generate` command.
This is still in trial period and will be finished probably after Savon 3 is more stable as
it can make use of its generation of a sample request.

## Usage

### Overall using gem

* SOAP - this uses Savon behind the scenes. Some defaults are overridden. Please see 'soap_handler.rb'-'default_options' method
for such defaults. When describing an API override this in 'savon_options' method  
* REST - this uses the resource class from the Rest-Client gem behind the scenes.

See [spec](spec) and [features](features) for example of usage.

### ExchangeHandler

To start with, create a class inheriting from a ‘Handler’ class for each web service that needs testing.
In this class you define the common parameters used for testing it.

For example:

```ruby
# Classes are set up through inheriting from either `Soaspec::RestHandler` or `Soaspec::SoapHandler`
class PuppyService < Soaspec::RestHandler
  # Set default headers for all `Exchanges` using this class
  headers accept: 'application/json', content_type: 'application/json'

  # URL for which all requests using this class will start with
  base_url 'http://petstore.swagger.io/v2/pet'

  # Accessing parts of a response

  # Define a method 'id' that can be obtained with either XPATH '//id' or JSONPath '$..id'
  element :id, :id
  # Define method to obtain a category id through JSON Path
  element :category_id, '$..category.id'
end
```

> You can easily create a exchange handler with the `soaspec add` command. This will also add comments explaining common methods that can be used

### Exchange

After creating the `ExchangeHandler`, you reference this class in creating `Exchange`s (objects that each represent a request / response pair).
Upon initialization of the Exchange object (or later on through setters), parameters specific to this request are set.
Most getters of the `Exchange` are on the response & will implicitly trigger the API request to be made.
Once this request has been made, all following accessors of the response will just use the response of the previous request made.

For example, to create a http post using the above `ExchangeHandler` and get parts of it's response.
```ruby
# Create a new Exchange that will post to 'http://petstore.swagger.io/v2/pet' with JSON { "status": "sold" }
# The 'body' key will convert it's value from a Hash to JSON
exchange = PuppyService.post(body: { status: 'sold' })

# This will trigger the request to be made & return the response, in this case a RestClient::Response object
response = exchange.response
# This will reuse the response already received return a value at JSON path $..category.id, throwing an exception if not found
exchange.category_id
# This will do the same but using path directly in code
exchange['$..category.id']
# Get the HTTP status code of the response
exchange.status_code
```

See [Request Body Parameters](https://gitlab.com/samuel-garratt/soaspec/wikis/RequestBodyParameters) for more details on setting a request body.
See [Creating an Exchange](https://gitlab.com/samuel-garratt/soaspec/wikis/CreatingExchange) for details on how to create an Exchange.

### Execute single REST via CMD line 

`soaspec exec` can be used to execute single REST requests. 
A docker image is built for convenience. An example to perform a get request 

```
docker run --rm -it registry.gitlab.com/samuel-garratt/soaspec exec get TEST_URL \n
 --token-url=https://login.salesforce.com/services/oauth2/token \n
 --client-id=CLIENT_ID --client-secret=SECRET --password=PASSWORD --username=USER
```

### Virtual Server

Soaspec includes a virtual server that returns REST and SOAP responses that can be used when learning or experimenting with API testing.
The gem itself uses this to test it's own functionality.

To start the server, after installing the gem, type

```
soaspec virtual_server [port_num]
```

By default it runs on port 4999. This will be used if `port_num` is empty.

You can look at the documentation for the web services provided at the rool url (e.g `localhost:4999`).

### RSpec

For example:

```ruby
context PuppyService.new('Order Puppies') do
  describe post(:create_pet, body: { status: 'sold' }) do # Post with status as sold in request
    its(['status']) { is_expected.to eq 'sold' } # Check responses status is sold
  end
end
```

#### Tips

If you find having a large backtrace on errors or RSpec shared examples such as 'success scenarios' this can shorten the
backtrace.

RSpec.configure do |config|

  config.backtrace_exclusion_patterns = [
    /rspec/
  ]
end

### Cucumber

If you're using `Cucumber` for testing a single API call then I would 
recommend the following

In the `Given` (or background) specify the `Exchange` object.
Either store this as an instance variable (e.g `@exchange`) or use the global `Soaspec.last_exchange` (which is automatically set).

In the `When`, use the `call` method to make the request `@exchange.call`. If problems occur in making the request this should separate such failures from issues with the response.

In the `Then`, make the assertions from the `@exchange` object.
E.g
```ruby
expect(@exchange['message']).to include 'success'
expect(@exchange.status_code).to eq 200
```

### Authentication

See [the wiki](https://gitlab.com/samuel-garratt/soaspec/wikis/Authentication) for details.

## Logging

By default traffic is logged to a file in the `logs` folder called `traffic_DATETIME.log`.

The `SpecLogger` class is responsible for handling all the logs.

To change the output file location to 'custom.log':
```ruby
Soaspec::SpecLogger.traffic_file = 'custom.log'
```

To change the folder the default log is:

```ruby
Soaspec::SpecLogger.traffic_folder = 'custom_folder'
```

To disable logging to a file set the 'output_to_file' to 'false'.
```ruby
Soaspec::SpecLogger.output_to_file = false
```
There is a similar parameter to logging to the terminal, STDOUT (by default false).
You can also change the color of this output with the `terminal_color` attribute.
```ruby
Soaspec::SpecLogger.output_to_terminal = true
Soaspec::SpecLogger.terminal_color = :magenta
```

## Learning more

Looking at the website [here](https://samuel-garratt.gitlab.io/soaspec/)

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/samuel-garratt/soaspec. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Soaspec project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/samuel-garratt/soaspec/blob/master/CODE_OF_CONDUCT.md).
