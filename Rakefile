# frozen_string_literal: true

require 'bundler/gem_tasks'
require 'rspec/core/rake_task'
require 'rake/clean'
require 'yard'
require 'yard/doctest/rake'

ENV['folder'] ||= ''
ENV['test'] ||= ''

desc 'Run tests'
RSpec::Core::RakeTask.new(spec: %i[clean clobber start_test_server]) do |t|
  t.pattern = "spec/**/#{ENV['folder']}*/#{ENV['test']}*_spec.rb"
end

task parallel: %i[clean clobber start_test_server] do
  puts `parallel_rspec spec`
end

task default: :spec

CLEAN.include 'tmp/*'
CLOBBER.include 'logs/*'

desc 'Start virtual web service'
task :start_test_server do
  ENV['leave_server_running'] = 'true' # This will stop created suites from killing this service
  mkdir_p 'logs'
  ENV['test_server_pid'] = Process.spawn('ruby', 'exe/soaspec', 'virtual_server', err: %w[logs/test_server.log w]).to_s
  sleep 2 # Wait a little for virtual server to start up
  puts 'Running test server at pid ' + ENV['test_server_pid']
end

YARD::Rake::YardocTask.new do |t|
  t.files = %w[features/**/*.feature features/**/*.rb lib/soaspec/cucumber/*.rb] # lib/soaspec/cucumber/*.rb]
end

desc 'Ensure system has all docs'
task :must_have_docs do
  yard = `yard`
  puts yard
  percentage = yard.lines.last.strip.split('%').first.to_f
  raise 'Must be fully documented' unless percentage == 100.00
end

desc 'Test examples shown in documentation'
YARD::Doctest::RakeTask.new do |task|
  task.doctest_opts = %w[-v]
  # task.pattern = 'lib/**/*.rb'
  task.pattern = 'lib/soaspec/core_ext/hash.rb'
end
