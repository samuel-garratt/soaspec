* Exception in setting a value results in 'headers' being called on Nil Class
* `yard` should show everything documented
* Rubocop should have 0 offenses
* Unit tests
  * OAuth class, etc
* Get initial `soaspec new` working with TODOs as placeholders for how to get started
* Request method from within exchange
  * Use this in tests
* Basic service generator
* Give examples and convenience methods for building classes for each SOAP or REST operation
* Potentially have in built use of 'vcr' and 'http_stub' gems
* Handle proxies to record traffic for MiddleWare testing
* soaspec generate
  * Get wsdl generator working for non complex gems (Put on hold til new Savon version)
  * Generate from a RAML
* Much more - please raise an issue for suggestion