Feature: Ordering Puppies

  A member of the public who visits a pet store should be able to order a puppy and receive it

  Scenario: Order Puppy
    Given I am ordering a puppy
    And the puppy's name is Charlie
    When I make the order
    Then the puppy I receive should be named Charlie
    And he should be sold to me

  Scenario: Generic post
    Given I am performing a post on the 'PuppyService' API
    And I set the name to 'Charlie'
    And I set the status to 'sold'
    When I make the request
    Then it should have the name 'Charlie'
    And it should have the status 'sold'

  Scenario: Generic Get
    Given I am performing a get on the 'puppy service' API
    And I use the path findByStatus
    And I filter 'status' by 'sold'
    And I set header 'accept' to 'application/xml'
    When I make the request
    Then it should be successful