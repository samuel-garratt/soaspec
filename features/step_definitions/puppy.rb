# frozen_string_literal: true

require 'soaspec'

# Class representing the Puppy
class PuppyService < Soaspec::RestHandler
  headers accept: 'application/json', content_type: 'application/json'
  base_url 'https://petstore.swagger.io/v2/pet'

  element :category_id, '$..category.id'
end

# Will create a new Puppy Exchange
Given(/^I am ordering a puppy$/) do
  PuppyService.new('Order Puppies').use
  DataMagic.load 'default.yml'
  @default_data = data_for(:pet)
  @order = post(:create_pet, body: @default_data)
end

And("the puppy's name is {word}") do |name|
  @order.name = name
end

When(/^I make the order$/) do
  @order.call
end

Then('the puppy I receive should be named {word}') do |name|
  expect(@order[:name]).to eq name
end

And(/^he should be sold to me$/) do
  expect(@order[:status]).to eq 'sold'
end
