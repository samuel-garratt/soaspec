# frozen_string_literal: true

# This includes settings used in general for the Cucumber scenarios
require 'rspec'
require 'data_magic'
require 'soaspec'
require 'soaspec/cucumber/generic_steps'

World DataMagic # Used as example of loading data smartly
World Soaspec::RestMethods # Only include this if you want to use REST 'get', 'post' methods defined

Soaspec::SpecLogger.output_to_terminal = true
