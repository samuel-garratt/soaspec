# frozen_string_literal: true

require 'bundler/setup'
require 'data_magic'

After do
  delete @id
end
