# frozen_string_literal: true

require 'rest-client' # REST
require 'erb' # Embedded ruby
require 'yaml' # Reading yaml
require 'rspec' # Testing framework
require 'rspec/its'
require 'savon' # SOAP
require 'nokogiri' # XPath
require 'date'
require 'jsonpath'

require 'soaspec/version'
require 'soaspec/indifferent_hash'
require 'soaspec/o_auth2'
require 'soaspec/template_reader'
require 'soaspec/exchange_handlers/soap_handler'
require 'soaspec/exchange_handlers/exchange_handler'
require 'soaspec/exchange_handlers/rest_methods'
require 'soaspec/exchange/exchange'
require 'soaspec/matchers'
require 'soaspec/soaspec_shared_examples'
require 'soaspec/core_ext/hash'
require 'soaspec/spec_logger'
require 'soaspec/exe_helpers'
require 'soaspec/exchange_handlers/rest_handler'
require 'soaspec/exchange_handlers/handler_accessors'
require 'soaspec/interpreter'
require 'soaspec/errors'
require 'soaspec/wait'
require 'soaspec/baseline'

# Gem for handling SOAP and REST api tests
module Soaspec
  @template_folder = 'templates'
  @auto_oauth = true
  @log_warnings = true
  # @return [String] Folder in which credentials are stored
  @credentials_folder = 'credentials'
  # @return [ExchangeHandler] handler A class inheriting from Soaspec::ExchangeHandler.
  #   Exchange class uses this to determine how to work
  @api_handler = nil

  class << self
    # Folder used to store templates for API calls
    # @return [String]
    attr_reader :template_folder
    # Stores last exchange
    # @return [Exchange]
    attr_accessor :last_exchange
    # Automatically add Authorization header to RestHandler where oauth2 credentials are specified
    # @return [Boolean] Whether to add authorization header
    attr_accessor :auto_oauth

    # Folder used to store templates for API calls
    # Converts folder / folders into an array depending upon string passed
    def template_folder=(folder)
      @template_folder = folder.include?('\\') ? folder.split('\\') : folder.split('/')
    end

    # Credentials folder used to store secret data (not in source control) E.g passwords
    # Used in oauth2_file command
    # @return [String] Folder in which credentials are stored
    attr_accessor :credentials_folder

    # Used so that exchange class knows what context it's in.
    # @return [ExchangeHandler] handler A class inheriting from Soaspec::ExchangeHandler. Exchange class uses this
    attr_writer :api_handler

    # @return [ExchangeHandler] handler A class inheriting from Soaspec::ExchangeHandler. Exchange class uses this
    def api_handler
      unless @api_handler
        raise Soaspec::Error, '@exchange_handler not set. ' \
        'Set either with `Soaspec.api_handler = Handler.new` or within the exchange'
      end
      @api_handler
    end

    # Set whether to transform strings to keys in request automatically.
    # @return [Boolean]
    attr_writer :always_use_keys

    # @return [Boolean] Whether to log warnings such as methods that may change usage in the future
    attr_accessor :log_warnings

    # @return [Boolean] Whether to transform strings to keys in request automatically
    def always_use_keys?
      @always_use_keys || true
    end

    # @return [Boolean] Whether to see params sent to & received from oauth URL
    def debug_oauth?
      puts 'Soaspec.debug_oauth? now deprecated. Please use Soaspec::OAuth2.debug_oauth? instead'
      Soaspec::OAuth2.debug_oauth?
    end

    # Specify whether to see params sent to and retrieved from oauth.
    # This will put password in log file, only recommended for debugging
    # @param [String] set Whether to debug oauth
    def debug_oauth=(set)
      puts 'Soaspec.debug_oauth= now deprecated. Please use Soaspec::OAuth2.debug_oauth= instead'
      Soaspec::OAuth2.debug_oauth = set
    end

    # Whether to log all API traffic
    # @param [Boolean] set
    def log_api_traffic=(set)
      puts 'Soaspec.log_api_traffic= now deprecated. Please use Soaspec::SpecLogger.log_api_traffic= instead'
      Soaspec::SpecLogger.log_api_traffic = set
    end

    # @return [Boolean] Whether to log all API traffic
    def log_api_traffic?
      puts 'Soaspec.log_api_traffic? now deprecated. Please use Soaspec::SpecLogger.log_api_traffic? instead'
      Soaspec::SpecLogger.log_api_traffic?
    end
  end
end
