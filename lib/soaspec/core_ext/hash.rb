# frozen_string_literal: true

# Override Hash class with convenience methods
class Hash
  # Transform each key in Hash to a symbol. Privately used by non-self method
  # @param [Object] value Value inside hash to transform keys under
  def self.transform_keys_to_symbols(value)
    return value unless value.is_a?(Hash)

    hash = value.each_with_object({}) { |(k, v), memo| memo[k.to_sym] = Hash.transform_keys_to_symbols(v); }
    hash
  end

  # Take keys of hash and transform those to a symbols
  # @example
  #   hash = { 'a' => 1, 'b' => { c: 4 } }
  #   hash.transform_keys_to_symbols # => { a: 1, b: { c: 4 } }
  # @return [Hash] Hash will all keys converted to symbols
  def transform_keys_to_symbols
    each_with_object({}) { |(k, v), memo| memo[k.to_sym] = Hash.transform_keys_to_symbols(v); }
  end

  # Value present in nested Hash.
  # @example
  #   hash = { a: { b: 25 }, c: 3 }
  #   hash.include_value?(25) #=> true
  # @return [Boolean] Whether value is included in nested Hash
  def include_value?(value)
    each_value do |v|
      return true if v == value
      next unless v.is_a? Hash

      v.each_value do |v|
        return true if v == value
        next unless v.is_a? Hash

        v.each_value do |v|
          return true if v == value
        end
      end
    end
    false
  end
end
