# frozen_string_literal: true

require 'active_support/core_ext/string/inflections'

# @return [Exchange] Return current or last exchange used in Cucumber
def current_exchange
  @current_exchange ||= Soaspec.last_exchange
end

# Pass in the operation (HTTP method or SOAP operation) in first parameter and api name as second.
# API name can be mulitple words and it will be converted to camel case to find the ExchangeHandler class
# @example Create an Exchange representing an HTTP 'post' based on the 'Puppy' RestHandler class
#   I am performing a post on the Puppy API
# @example Create an Exchange for a 'get_bank' operation on for a 'Banking' SoapHandler class
#   I am performing a get_bank on the Banking API
Given 'I am performing a {word} on the {string} API' do |operation, api_name|
  @current_exchange = api_name.tr(' ', '_').camelize.constantize.send operation
end

# Set a parameter in the request body
# @example Set the name element in the request body to Charlie
#   And I set the name to 'Charlie'
# @param [String] Element in request body to set
# @param [String] Value to set it to
Given 'I set the {word} to {string}' do |key, value|
  current_exchange[key] = value
end

# Add onto the base_url to make a complete url for the test
# @example base_url is 'http://petstore.swagger.io/v2' and I want a post to 'http://petstore.swagger.io/v2/pet'
#   I use the path pet
Given 'I use the path {word}' do |suburl|
  current_exchange.suburl = suburl
end

# Add onto the base_url to make a complete url for the test
# @example base_url is 'http://petstore.swagger.io/v2' and I want a post to 'http://petstore.swagger.io/v2/basic.json'
#   I use the path 'basic.json'
Given 'I use the path {string}' do |suburl|
  current_exchange.suburl = suburl
end

# Add a query parameter for http 'get' requests. e.g. will add '?filter_key=filter_value' onto URL
# @example To create a query for '?status=sold'
#   And I filter 'status' by 'sold'
# @example To add a query for 'area' being 'australia' to make a total of '?status=sold&area=austrialia'
#   And I filter 'area' by 'australia'
Given 'I filter {string} by {string}' do |filter_key, filter_value|
  transformed_key = filter_key.to_sym
  if current_exchange.override_parameters[:q]
    current_exchange.override_parameters[:q][transformed_key] = filter_value
  else
    current_exchange.override_parameters[:q] = { transformed_key => filter_value }
  end
end

# Add HTTP header 'header_name' as 'header_value'
# @example
#   And I set header 'accept' to 'application/xml'
Given 'I set header {string} to {string}' do |header_name, header_value|
  if current_exchange.override_parameters[:params]
    current_exchange.override_parameters[:params][header_name] = header_value
  else
    current_exchange.override_parameters[:params] = { header_name => header_value }
  end
end

# Make the API call. This is automatically done when any method extracting a response is made. It can be done
# explicitly here as it is a meaningful step
When 'I make the request' do
  current_exchange.call
end

# Extract the value from the response that is at the path 'key' and verify it is eq to expected_value
# @example Assert response body has at the path 'name', the value 'Charlie'
#   Then it should have the name 'Charlie'
Then 'it should have the {word} {string}' do |key, expected_value|
  expect(current_exchange[key]).to eq expected_value
end

# Extract the value from the response that is at the path 'key_string' and verify it is eq to expected_value
# Conversion is made on key_string to make it one snake case word
# @example Assert response body has at the path 'customer_name', the value 'Charlie'
#   Then it should have the 'customer name' 'Charlie'
Then 'it should have the {string} {string}' do |key_string, expected_value|
  key = key_string.tr(' ', '_')
  actual_value = current_exchange.respond_to?(key) ? current_exchange.send(key) : current_exchange[key]
  expect(actual_value.to_s).to eq expected_value
end

# Verify that the HTTP status code is 200..299 and that any defined mandatory elements / mandatory values are as expected
Then 'it should be successful' do
  expect(current_exchange).to be_successful
end
