# frozen_string_literal: true

# Load this file for demoing Soaspec (e.g in IRB). Has common settings applicable for demoing

require_relative '../soaspec'
Soaspec::SpecLogger.output_to_terminal = true
