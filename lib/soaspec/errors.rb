# frozen_string_literal: true

module Soaspec
  # Standard Error related to Soaspec
  class Error < StandardError; end
  # Error related to a response
  class ResponseError < Error; end
  # Error related to not having a recorded baseline yet
  class BaselineError < Error; end
end

# Raised to represent when there's no element at an Xpath
class NoElementAtPath < Soaspec::Error
  def initialize(msg = 'No element at path found')
    super
  end
end

# Did not find any element by provided key in the Hash
class NoElementInHash < Soaspec::Error
  def initialize(msg = 'No element in Hash found')
    super
  end
end
