# frozen_string_literal: true

module Soaspec
  # Convenience methods to set Exchange specific properties
  # Will be used when creating a subclass of Exchange
  module ExchangeProperties
    # Set default exchange handler for this exchange
    #
    # @example Custom Exchange sub class always using particular exchange handler
    #   class CustomExchange < Exchange
    #     default_handler EchoService
    #   end
    #   CustomExchange.exchange_handler.class # => EchoService
    #
    # @example Custom Exchange that has handler with particular properties
    #   class PreExchange < Exchange
    #     default_handler BLZService, 'Pre Exchange', operation: :get_bank
    #   end
    #   PreExchange.new.request_parameters.operation # => :get_bank
    #
    # This is helpful for when you need a new exchange handler created for each exchange
    # @param [< ExchangeHandler] handler_class Class of ExchangeHandler to set Exchange to use
    # @param [String] name Name to call handler when it's instantiated (Defaults to class name)
    # @param [Hash] params Hash of parameters to set for instance of ExchangeHandler
    def default_handler(handler_class, name = handler_class.to_s, params = {})
      define_method('default_handler_used') do
        params_used = Hash[params.map do |k, param|
          [k, param.is_a?(String) ? ERB.new(param).result(binding) : param]
        end]
        handler_class.new name, params_used
      end
    end

    # Set retry_for_success to true, retrying response until a successful status code is returned
    # @param [Integer] retry_count Times to retry to get a positive response
    def expect_positive_status(retry_count: 3)
      define_method('retry_count') { retry_count }
      define_method('retry_for_success?') { true }
    end
  end
end
