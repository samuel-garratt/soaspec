# frozen_string_literal: true

module Soaspec
  # Ways of repeating an exchange to reach a desired outcome
  module ExchangeRepeater
    # Wait until the passed block returns true
    # @param [Hash] opts Options for this instance
    # @option opts [Numeric] :timeout (5) Seconds to wait before timing out.
    # @option opts [Numeric] :interval (0.2) Seconds to sleep between polls.
    # @option opts [String] :message Exception message if timed out.
    # @option opts [Array, Exception] :ignore Exceptions to ignore while polling (default: Error::NoSuchElementError)
    # @return [Exchange] Returns itself so operations can be done on the exchange after it's done waiting
    def until(opts = {}, &script)
      Soaspec::Wait.until(opts) do
        @response = nil # Reset response so it can be made repeatedly
        instance_eval(&script)
      end
      self
    end
  end
end
