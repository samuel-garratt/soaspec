# frozen_string_literal: true

module Soaspec
  # Methods to help build a Request
  module RequestBuilder
    # Specify a url to add onto the base_url of the ExchangeHandler used
    # @param [String] url Url to add onto the base_url of the ExchangeHandler used
    def suburl=(url)
      @override_parameters[:suburl] = url
    end

    # Specify HTTP method to use. Default is :post
    # @param [Symbol] method HTTP method. E.g, :get, :patch
    def method=(method)
      @override_parameters[:method] = method
    end

    # Set a parameter request in the request body.
    # Can be used to build a request over several steps (e.g Cucumber)
    # Will be used with FactoryBot
    #
    # @example
    #   exchange['name'] = 'tester'
    #   # Will set { name: tester } in the response, formatting as JSON or XML depending on REST / SOAP
    # @param [String, Symbol] key Name of request element to set
    # @param [String] value Value to set request element to
    def []=(key, value)
      @override_parameters[:body] ||= {}
      @override_parameters[:body][key] = value
    end

    # Implement undefined setter with []= for FactoryBot to use without needing to define params to set
    # @param [Object] method_name Name of method not defined
    # @param [Object] args Arguments passed to method
    # @param [Object] block Block passed to method
    def method_missing(method_name, *args, &block)
      set_value = args.first
      if method_name[-1] == '=' # A setter method
        getter_name = method_name[0..-2]
        if set_value.class < Exchange # This would be prerequisite exchange
          define_singleton_method(getter_name) { set_value }
          self[getter_name] = set_value.id if set_value.respond_to?(:id)
        else
          self[getter_name] = set_value
        end
      else
        super
      end
    end

    # Used for setters that are not defined
    # @param [String] method_name Name of method
    # @param [Array] args List of arguments to method
    def respond_to_missing?(method_name, *args)
      method_name[-1] == '=' || super
    end

    # Makes request, caching the response and returning self
    # Used by FactoryBot
    # @return [Self] Return itself so methods can be called on itself afterwards
    def save!
      @retry_for_success = @fail_factory ? false : true
      call
    end

    # Add request parameters onto existing exchange
    # @example New query of api_key=123 to exchange
    #   exchange = Exchange.new
    #   exchange + { q: { api_key: 123 } }
    #
    # @example Add query of testing as true to exchange
    #   exchange = Exchange.new q: { api_key: 123 }
    #   exchange + { q: { testing: true } }
    #   exchange.request_parameters.q # => { api_key: 123, testing: true } }
    #
    # @example Add suburl of 'test' to existing suburl
    #   exchange = Exchange.new suburl: 'subpath'
    #   exchange + { suburl: '/test' }
    #   exchange.request_parameters.suburl # => 'subpath/test'
    #
    # @example Using add to add suburl
    #   exchange = Exchange.new suburl: 'subpath'
    #   exchange.add suburl: '/test'
    #   exchange.request_parameters.suburl # => 'subpath/test'
    # @param [Hash] request_parameters Parameters to add to exchange
    def +(request_parameters)
      request_parameters.each do |req_name, req_value|
        if req_value.respond_to?(:each)
          req_value.each do |key, value|
            if @override_parameters[req_name]
              @override_parameters[req_name][key] = value
            else
              @override_parameters[req_name] = { key => value }
            end
          end
        else
          if @override_parameters[req_name]
            @override_parameters[req_name] += req_value
          else
            @override_parameters[req_name] = req_value
          end
        end
      end
    end

    alias add +
  end
end
