# frozen_string_literal: true

module Soaspec
  # Enables Exchange to store and retrieve variables so that one
  # exchange can make assertions based on a previous one
  module VariableStorer
    # Stores a value in the api handler that can be accessed by the provided name
    # @param [Symbol] name Name of method to use to access this value within handler
    # @param [String] value Path to value to store
    def store(name, value)
      exchange_handler.store(name, self[value])
    end

    # Retrieve the stored value from the Api Handler
    # @param [String, Symbol] name Name of value to retrieve
    # @return [Object] value from the Api Handler stored previously
    def retrieve(name)
      method = '__stored_val__' + name.to_s
      raise ArgumentError('Value not stored at ') unless exchange_handler.respond_to? method

      exchange_handler.send(method)
    end
  end
end
