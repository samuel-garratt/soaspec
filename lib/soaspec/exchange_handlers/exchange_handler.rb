# frozen_string_literal: true

require_relative 'handler_accessors'
require_relative 'exchange_handler_defaults'

module Soaspec
  # Inherit this for a class describing how to implement a particular exchange.
  # Has methods common to Soaspec framework for working with Exchange/Handler pair
  class ExchangeHandler
    extend Soaspec::HandlerAccessors
    include ExchangeHandlerDefaults

    # @return [Exception] Exception if raised
    attr_accessor :exception

    # Use an instance of this ExchangeHandler in any further Exchange's
    # This is a convenience method as it creates an ExchangeHandler behind the scenes
    # @return [ExchangeHandler] Exchange handler instance created
    def self.use
      new.use
    end

    # @return [String] Name of the template file to be used in the API request
    attr_reader :template_name
    # @return [Symbol] Option used to generate Request Body. Either :hash or :template
    attr_accessor :request_option

    # Explicitly defined elements for which a path has been predefined
    def elements
      public_methods.select { |i| i[/__custom_path_.+/] }
    end

    # Set instance variable name
    # @param [String, Symbol] name Name used when describing API test
    # @param [Hash] _options Parameters defining handler. Used in descendants
    def initialize(name = self.class.to_s, _options = {})
      use
      self.request_option = :hash
      raise ArgumentError, 'Cannot define both template_name and default_hash' if respond_to?(:template_name_value) && respond_to?(:default_hash_value)

      @template_name = respond_to?(:template_name_value) ? template_name_value : ''
      @default_hash = respond_to?(:default_hash_value) ? default_hash_value : {}
      @name = name
    end

    # Set Api handler used by Exchange class to this handler
    # @return [Self]
    def use
      Soaspec.api_handler = self
      self
    end

    # Sets api handler variable globally. This is used in 'Exchange' class
    # @return [String] Name set upon initialisation
    def to_s
      use
      @name.to_s
    end

    # Set the default hash representing data to be used in making a request
    # This will set the @request_option instance variable too
    # @param [Hash] hash Hash to send in request body by default
    def default_hash=(hash)
      self.request_option = :hash
      @default_hash = Soaspec.always_use_keys? ? hash.transform_keys_to_symbols : hash
    end

    # Set the request option type and the template name
    # Erb is used to parse the template file, executing Ruby code in `<%= %>` blocks to work out the final request
    # @param [String] name Name of file inside 'template' folder excluding extension
    def template_name=(name)
      self.request_option = :template
      @template_name = name
    end

    # Stores a value in a method that can be accessed by the provided name
    # @param [Symbol] name Name of method to use to access this value within handler
    # @param [String] value Value to store
    def store(name, value)
      define_singleton_method('__stored_val__' + name.to_s) { value }
    end

    # Set instance variable for each key in hash remove it from Hash
    # @param [Hash] hash with items to remove from
    # @param [Array] keys List of keys to remove from hash, setting each one
    def set_remove_keys(hash, keys)
      keys.each { |key| set_remove_key(hash, key) }
    end

    # Set instance variable and remove it from Hash
    # @param [Hash] hash Hash to remove/retrieve keys from
    # @param [String,Symbol] key Key to remove and to set instance variable for
    def set_remove_key(hash, key)
      return unless hash.key? key

      __send__("#{key}=", hash[key])
      hash.delete key
    end
  end
end
