# frozen_string_literal: true

# Default values set for methods on ExchangeHandler
module ExchangeHandlerDefaults
  # Will be used in 'success_scenarios' shared examples.
  # Set though 'mandatory_elements' method
  # @return [Array] Array of symbols specifying element names
  def expected_mandatory_elements
    []
  end

  # Change this through 'mandatory_xpath_values' method to specify xpath results that must be present in the response
  # Will be used in 'success_scenarios' shared examples
  # @return [Hash] Hash of 'xpath' => 'expected value' pairs
  def expected_mandatory_xpath_values
    {}
  end

  # Change this through 'mandatory_json_values' method to specify json results that must be present in the response
  # Will be used in 'success_scenarios' shared examples
  # @return [Hash] Hash of 'json/path' => 'expected value' pairs
  def expected_mandatory_json_values
    {}
  end

  # @return [Boolean] Whether all xpaths will be done with XML that is converted to lower case
  def convert_to_lower?
    false
  end

  # @return [Boolean] Whether to remove namespaces in xpath assertion automatically
  def strip_namespaces?
    false
  end

  # Request of API call. Either intended request or actual request
  # @param [Object] response Response from calling exchange
  def request(response)
    return "Request not yet sent Request option is #{@request_option}" unless response

    'Specific API handler should implement this'
  end

  # Set through 'retry_on_exceptions' method
  # @return [Array] List of exceptions to retry for
  def retry_on_exceptions
    []
  end

  # Set this through 'retry_on_exceptions' on ExchangeHandler
  # @return [Integer] Time to wait before retrying each request. Default of 1
  def retry_pause_time
    1
  end

  # Set this through 'retry_on_exceptions' on ExchangeHandler
  # @return [Integer] Times to retry before raising exception. Default of 3
  def retry_exception_limit
    3
  end
end
