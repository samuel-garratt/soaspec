# frozen_string_literal: true

module Soaspec
  # Describes methods test handlers use to easily set attributes
  # Some are included in 'success scenarios' and to configure the request sent
  module HandlerAccessors
    # Defines expected_mandatory_elements method used in 'success_scenario' shared examples
    # to indicate certain elements must be present
    # @param [Array] elements Array of symbols specifying expected element names for 'success scenario' in snakecase
    #
    # @example Inside class
    #   mandatory_elements :GetWeatherResult
    #
    # Or for a list
    #
    # @example Inside class
    #   mandatory_elements [:GetWeatherResult, :GetResultStatus]
    #
    # In test
    # describe Exchange(:name) do
    #   it_behaves_like 'success scenario' # Includes checks for mandatory elements
    # end
    def mandatory_elements(elements)
      define_method('expected_mandatory_elements') do
        return [elements] if elements.is_a?(String) || elements.is_a?(Symbol)

        elements
      end
    end

    # Defines mandatory xpaths value pairs to be included in 'success scenario' shared example
    #
    # @example Inside class
    #   mandatory_xpath_values '//xmlns:GetWeatherResult' => 'Data Not Found'
    #
    # @example In test
    #   describe Exchange(:name) do
    #     it_behaves_like 'success scenario' # Includes xpath pair validation
    #   end
    #
    # @param [Hash] xpath_value_pairs Hash of element => expected value that must appear
    #                                 in a successful response body
    def mandatory_xpath_values(xpath_value_pairs)
      raise ArgumentError('Hash of {xpath => expected values} expected ') unless xpath_value_pairs.is_a? Hash

      define_method('expected_mandatory_xpath_values') { xpath_value_pairs }
    end

    # Defines mandatory json path value pairs to be included in 'success scenario' shared example
    #
    # @example Inside class
    #   mandatory_json_values '$..GetWeatherResult' => 'Found'
    #
    # @example In test
    #   describe Exchange(:name) do
    #     it_behaves_like 'success scenario' # Includes json pair validation
    #   end
    #
    # @param [Hash] json_value_pairs Hash of element => expected value that must appear
    #                                in a successful response body
    def mandatory_json_values(json_value_pairs)
      raise ArgumentError("Hash of {'jsonpath' => expected values} expected") unless json_value_pairs.is_a? Hash

      define_method('expected_mandatory_json_values') { json_value_pairs }
    end

    # Links a particular path to a meaningful method that can be accessed from Exchange class.
    # This will use the 'value_from_path' method which
    # should be implemented by each ExchangeHandler
    # @param [String, Symbol] name Method name used to access element
    # @param [String, Symbol] path Path to find object (e.g, XPath, JSONPath).
    #                              For JSONPath a ',' can be put to get an element either path
    def element(name, path)
      define_method("__custom_path_#{name}") do |response|
        value_from_path(response, path.to_s)
      end
    end

    # Links an attribute to a meaningful method that can be accessed from Exchange class.
    # This will use the 'value_from_path' method which
    # should be implemented by each ExchangeHandler
    # @param [String, Symbol] name Method name used to access attribute
    # @param [String, nil, Hash] attribute Attribute name to extract from xml. If not set, this will default to @name
    def attribute(name, attribute = nil)
      attribute_used = attribute || name.to_s
      define_method("__custom_path_#{name}") do |response|
        value_from_path(response, 'implicit', attribute: attribute_used)
      end
    end

    # All xpath will be done with XML that is converted to lower case
    # You must then use lower case in the xpath's to obtain the desired values
    # @param [Boolean] set Whether to convert all xml in response to lower case before performing XPath
    def convert_to_lower(set)
      return unless set

      define_method('convert_to_lower?') { true }
    end

    # Whether to remove namespaces from response in Xpath assertion automatically
    # For why this may not be a good thing in general see
    # http://tenderlovemaking.com/2009/04/23/namespaces-in-xml.html
    # This will be overridden if xpath has a ':' in it
    # @param [Boolean] set Whether to strip namespaces form XML response
    def strip_namespaces(set)
      return unless set

      # Whether to remove namespaces in xpath assertion automatically
      define_method('strip_namespaces?') { true }
    end

    # Set the default hash representing data to be used in making a request
    # This will set the @request_option instance variable too
    # @param [Hash] hash Default hash of request
    def default_hash(hash)
      define_method('default_hash_value') do
        @request_option = :hash
        Soaspec.always_use_keys? ? hash.transform_keys_to_symbols : hash
      end
    end

    # Set the request option type and the template name
    # Erb is used to parse the template file, executing Ruby code in `<%= %>` blocks to work out the final request
    # @param [String] name Name of file inside 'template' folder excluding extension
    def template_name(name)
      define_method('template_name_value') do
        @request_option = :template
        name
      end
    end
  end
end
