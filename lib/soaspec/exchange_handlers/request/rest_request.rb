# frozen_string_literal: true

module Soaspec
  # Models a request made to a REST API
  class RestRequest
    # @example GET method
    #   :get
    # @return [Symbol] REST method used
    attr_accessor :method
    # This will be the actual payload sent. This could be set in Exchange through the payload
    # explicitly, the 'body' param which will convert a Hash to JSON or a template
    # @example JSON body
    #   {"test":5}
    # @return [String] Body of request sent. Payload that will be sent in request
    attr_accessor :body
    # These can be used to verify components of a request are as expected or other operations that
    # are useful with a Hash
    # @return [Hash] Parameters that will be used to form request string
    attr_accessor :body_params
    # @return [String] Name given to test to describe it
    attr_accessor :test_name
    # @return [String] User used in basic auth
    attr_accessor :basic_auth_user
    # @return [String] Password used in basic auth
    attr_accessor :basic_auth_password
    # @return [String] Url appended to base_url
    attr_accessor :suburl
    # @return [Hash] Miscellaneous parameters
    attr_accessor :other_params
    # Headers. Keys that are `symbols` will be converted from `snake_case` to `Word-Word2`
    # @return [Hash] Headers sent as part of request
    attr_accessor :headers
    # @return [Soaspec::RestHandler] RestHandler used for this request
    attr_accessor :rest_handler
    # @return [Integer] Maximum number of times to retry for a socket error
    MAX_SOCKET_RETRIES = 3
    # @return [Integer] Attempts to try socket
    attr_accessor :socket_tries

    # @return [String] Base url plus sub url
    def full_url
      url = rest_handler.base_url_value
      url = concat_urls(url, suburl) if suburl
      @full_url = ERB.new(url).result(binding)
    end

    # Interpret REST parameters given provided parameters and adding defaults, making
    # transformations
    #
    # @param [Hash] request_parameters Parameters used in making a request
    # @return [Hash] Request parameters merged with default values
    def interpret_parameters(request_parameters)
      request_parameters = request_parameters.dup # Must duplicate hash as deletion occurring
      request_parameters[:params] ||= {}
      request_parameters[:method] ||= :post
      suburl = request_parameters[:suburl]
      if suburl
        request_parameters[:suburl] = if suburl.is_a? Array
                                        suburl.collect(&:to_s).join('/')
                                      else
                                        suburl.to_s
                                      end
      end
      # Use q for query parameters. Nested :params is ugly, long and unclear
      request_parameters[:params] = request_parameters.delete(:q) if request_parameters[:q]
      request_parameters
    end

    # @param [Hash] overall Overall parameters used in Request
    # @param [Hash] options Headers and basic auth options
    # @param [< Soaspec::RestHandler] rest_handler RestHandler handling creation of this request
    def initialize(overall, options, rest_handler)
      self.socket_tries = 0
      self.rest_handler = rest_handler
      overall_params = interpret_parameters(overall)
      @overall_params = overall_params.dup
      self.method = overall_params.delete(:method)
      self.body = post_data
      overall_params.delete(:body)
      overall_params.delete(:payload)
      self.suburl = overall_params.delete(:suburl)
      self.test_name = overall_params.delete(:name)
      self.other_params = overall_params
      self.basic_auth_user = options[:user]
      self.basic_auth_password = options[:password]
      self.headers = options[:headers] # TODO: Use this in request
    end

    # @return [Hash] Query parameters for a REST Request
    def query
      other_params[:params]
    end

    # @param [String, Symbol] value Message to send to object retrieving a value
    # @return [Object] Result of retrieving value
    def [](value)
      send(value)
    end

    # @return [String] Show inspection of all parameters
    def to_s
      inspect
    end

    # @return [Array] Array containing parameters used in making a request
    def flatten
      [method] + [suburl].flatten + query.flatten
    end

    # @return [String] Description that could be used in filename
    def description
      suburl_desc = suburl.is_a?(Array) ? File.join(suburl) : suburl
      query_desc = ''
      query&.each do |key, value|
        query_desc = File.join(query_desc, "#{key}_#{value}")
      end
      components = [method.to_s, suburl_desc, query_desc, 'response']
      File.join(*components.collect!(&:to_s))
    end

    # Use the request parameters to call the REST api
    def call(merged_options)
      rest_handler.exception = nil # Remove any previously stored exception
      Soaspec::SpecLogger.info("request body: #{body}") if body
      merge_headers = (merged_options[:headers] || {}).merge(other_params)
      params = merged_options.merge(method: method, url: full_url,
                                    headers: merge_headers)
      params.merge!(payload: body) if body
      RestClient::Request.execute params
    rescue RestClient::Exception => e
      rest_handler.exception = e
      raise e unless e.respond_to? :response

      e.response
    rescue SocketError => e
      self.socket_tries += 1
      sleep 0.5
      retry unless socket_tries > MAX_SOCKET_RETRIES
      raise e
    end

    # Work out data to send based upon payload, template_name, or body
    # @return [String, NilClass] Payload to send in REST request. Nil if nothing is to be sent
    def post_data
      option = rest_handler.request_option
      body = if option == :hash && !@overall_params[:payload]
               self.body_params = rest_handler.hash_used_in_request(@overall_params[:body])
               @overall_params[:payload] = JSON.generate(body_params)
             elsif option == :template
               test_values = @overall_params[:body] ? @overall_params[:body].dup : nil
               self.body_params = test_values || @overall_params
               Soaspec::TemplateReader.new.render_body(rest_handler.template_name, body_params)
             else
               @overall_params[:payload]
             end
      return body if body && body != '{}'

      nil
    end

    private

    # Join two urls, removing double slashes if they're on end and beginning of path
    def concat_urls(url, suburl)
      url = url.to_s
      suburl = suburl.to_s
      if (url.slice(-1, 1) == '/') || (suburl.slice(0, 1) == '/')
        url + suburl
      else
        "#{url}/#{suburl}"
      end
    end
  end
end
