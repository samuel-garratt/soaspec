# frozen_string_literal: true

module Soaspec
  # Models a request made to a SOAP API
  class SoapRequest
    # @example SOAP operation
    #   :login
    # @return [Symbol] SOAP operation used
    attr_accessor :operation
    # Hash representing what will be sent to Savon. Either the exact xml (if using a template)
    # or a message Hash that will be converted to XML by Savon
    # @example
    #   { xml: "<env:Envelope xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" ...</env:Body>\n</env:Envelope> " }
    # @return [Hash] Body of request sent
    attr_accessor :body
    # @return [Symbol] Method of building the request (:hash, :template)
    attr_accessor :request_option
    # @return [String] Name given to test to describe it
    attr_accessor :test_name

    # @param [Symbol] operation Soap operation used
    # @param [Hash] body Hash with method of SOAP generated contained
    # @param [Symbol] request_option Method of building the request (:hash, :template)
    def initialize(operation, body, request_option)
      self.body = body
      self.operation = operation
      self.request_option = request_option
    end

    # @param [String, Symbol] value Message to send to object retrieving a value
    # @return [Object] Result of retrieving value
    def [](value)
      send(value)
    end

    # @return [String] Show inspection of all parameters
    def to_s
      inspect
    end
  end
end
