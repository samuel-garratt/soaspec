# frozen_string_literal: true

module Soaspec
  # Convenience methods for once off usage of a REST request
  module RestExchangeFactory
    # Make REST Exchange with 'post' method within this Handler context
    # @param [Hash, String] params Exchange parameters. If String is used it will be for the request payload
    # @option params [String] :name Name to appear in traffic logs
    # @option params [Hash] :params Extra parameters (E.g. headers)
    # @option params [String] :suburl URL appended to base_url of class
    # @option params [Hash] :q Query for REST
    # Following are for the body of the request
    # @option params [Hash] :body Hash to be converted to JSON in request body
    # @option params [String] :payload String to be passed directly in request body
    # @option params [String] :template_name Path to file to be read via ERB and passed in request body
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def post(params = {})
      perform_exchange_with(:post, params)
    end

    # Make REST Exchange with 'patch' method within this Handler context
    # @param [Hash, String] params Exchange parameters. If String is used it will be for the request payload
    # @option params [String] :name Name to appear in traffic logs
    # @option params [Hash] :params Extra parameters (E.g. headers)
    # @option params [String] suburl URL appended to base_url of class
    # @option params [Hash] :q Query for REST
    # Following are for the body of the request
    # @option params [Hash] :body Hash to be converted to JSON in request body
    # @option params [String] :payload String to be passed directly in request body
    # @option params [String] :template_name Path to file to be read via ERB and passed in request body
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def patch(params)
      perform_exchange_with(:patch, params)
    end

    # Make REST Exchange with 'put' method within this Handler context
    # @param [Hash, String] params Exchange parameters. If String is used it will be for the request payload
    # @option params [String] :name Name to appear in traffic logs
    # @option params [Hash] :params Extra parameters (E.g. headers)
    # @option params [String] :suburl URL appended to base_url of class
    # @option params [Hash] :q Query for REST
    # Following are for the body of the request
    # @option params [Hash] :body Hash to be converted to JSON in request body
    # @option params [String] :payload String to be passed directly in request body
    # @option params [String] :template_name Path to file to be read via ERB and passed in request body
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def put(params = {})
      perform_exchange_with(:put, params)
    end

    # Make REST Exchange with 'get' method within this Handler context.
    # If merely a string is passed it will be used as the URL appended to base_url (same as suburl). Otherwise a Hash is expected
    # @param [Hash, String] params Exchange parameters. If String is used it will be for suburl
    # @option params [String] :name Name to appear in traffic logs
    # @option params [String] :suburl URL appended to base_url of class
    # @option params [Hash] :params Extra parameters (E.g. headers)
    # @option params [Hash] :q Query for REST
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def get(params = {})
      perform_exchange_with(:get, params)
    end

    # Make REST Exchange with 'delete' method within this Handler context.
    # If merely a string is passed it will be used as the URL appended to base_url (same as suburl). Otherwise a Hash is expected
    # @param [Hash, String] params Exchange parameters. If String is used it will be for suburl
    # @option params [String] :name Name to appear in traffic logs
    # @option params [String] :suburl URL appended to base_url of class
    # @option params [Hash] :q Query for REST
    # @option params [Hash] :params Extra parameters (E.g. headers)
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def delete(params = {})
      perform_exchange_with(:delete, params)
    end

    private

    # Make REST Exchange within this Handler context
    # @param [Symbol] rest_method HTTP rest method to use
    # @param [Hash, String] params Exchange parameters.
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def perform_exchange_with(rest_method, params = {})
      params = determine_params_for(rest_method, params)
      params[:name] ||= rest_method.to_s
      exchange_params = { name: params[:name] }
      if params[:template_name]
        exchange_params[:template_name] = params[:template_name]
        params.delete :template_name
      end
      new(exchange_params)
      exchange = Exchange.new(params[:name], method: rest_method, **params)
      yield exchange if block_given?
      exchange
    end

    # @param [Symbol] method HTTP rest method to use
    # @param [Hash, String] params Exchange parameters.
    # @return [Hash] Exchange Parameters after setting shorthand parameters
    def determine_params_for(method, params)
      return params if params.is_a? Hash

      case method
      when :get, :delete
        { suburl: params.to_s }
      when :post, :put, :patch
        { payload: params.to_s }
      else
        raise "'#{params}' needs to be a 'Hash' but is a #{params.class}"
      end
    end
  end
end
