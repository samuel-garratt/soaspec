# frozen_string_literal: true

module Soaspec
  # Contains commonly used REST methods. Include this module in the spec where you want to use it
  #
  # TODO: For some reason from 'soaspec pry' sinatra always gets loaded instead of this
  #
  # @example
  #   include Soaspec::RestMethods
  #
  #   context CustomExchangeHandler.new('Create Notes') do
  #     post(:minimum_data, body: { subject: 'Minimal' }) do
  #       it_behaves_like 'success scenario'
  #       its(:subject) { is_expected.to eq 'Minimal' }
  #     end
  #
  #     post(:one_note, body: { subject: 'One', note: { 'Note 1' } }) do
  #       it_behaves_like 'success scenario'
  #       it 'has 1 note' do
  #         expect(get(subject.id).values_at_path('notes').count).to eq 1
  #       end
  #     end
  #   end
  module RestMethods
    # Make REST Post Exchange
    # @param [String] name Name of test displayed
    # @param [Hash] params Exchange parameters
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def post(name, params = {})
      Exchange.new(name, method: :post, **params)
    end

    # Make REST Patch Exchange
    # @param [String] name Name of test displayed
    # @param [Hash] params Exchange parameters
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def patch(name, params = {})
      Exchange.new(name, method: :patch, **params)
    end

    # Make REST Put Exchange
    # @param [String] name Name of test displayed
    # @param [Hash] params Exchange parameters
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def put(name, params = {})
      Exchange.new(name, method: :put, **params)
    end

    # Make REST Get Exchange
    # @param [String] name Name of test displayed
    # @param [Hash] params Exchange parameters
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def get(name, params = {})
      Exchange.new(name, method: :get, **params)
    end

    # Make REST Delete Exchange
    # @param [String] name Name of test displayed
    # @param [Hash] params Exchange parameters
    # @return [Exchange] Instance of Exchange class. Assertions are made by default on the response body
    def delete(name, params = {})
      Exchange.new(name, method: :delete, **params)
    end
  end
end
