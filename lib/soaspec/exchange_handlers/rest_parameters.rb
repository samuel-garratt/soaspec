# frozen_string_literal: true

module Soaspec
  # Methods to define parameters specific to REST handler
  module RestParameters
    # Defines method 'base_url_value' containing base URL used in REST requests
    # @param [String] url Base Url to use in REST requests. Suburl is appended to this
    def base_url(url)
      raise ArgumentError, "Base Url passed must be a String for #{self} but was #{url.class}" unless url.is_a?(String)

      define_method('base_url_value') { ERB.new(url).result(binding) }
      # URL used if subclassing handler that sets this previously
      define_singleton_method('parent_url') { ERB.new(url).result(binding) }
    end

    # Will create access_token method based on passed parameters
    # @param [Hash] params OAuth 2 parameters
    # @option params [token_url] URL to retrieve OAuth token from. @Note this can be set globally instead of here
    # @option params [client_id] Client ID
    # @option params [client_secret] Client Secret
    # @option params [username] Username used in password grant
    # @option params [password] Password used in password grant
    # @option params [security_token] Security Token used in password grant
    def oauth2(params)
      # @!method oauth_obj Object to handle oauth2
      define_method('oauth_obj') { OAuth2.new(params, api_username) }
      # @!method access_token Retrieve OAuth2 access token
      define_method('access_token') { oauth_obj.access_token }
      # @!method instance_url Retrieve instance url from OAuth request
      define_method('instance_url') { oauth_obj.instance_url }
    end

    # Pass path to YAML file containing OAuth2 parameters
    # @param [String] path_to_filename Will have Soaspec.credentials_folder appended to it if set
    def oauth2_file(path_to_filename)
      oauth_parameters = load_credentials_hash(path_to_filename)
      @client_id = oauth_parameters[:client_id] if oauth_parameters[:client_id]
      oauth2 oauth_parameters
    end

    # @return [String] Client id obtained from credentials file
    def client_id
      raise '@client_id is not set. Set by specifying credentials file with "oauth2_file FILENAME" before this is called' unless @client_id

      @client_id
    end

    # Define basic authentication
    # @param [String] user Username to use
    # @param [String] password Password to use
    def basic_auth(user: nil, password: nil)
      raise ArgumentError, "Must pass both 'user' and 'password' for #{self}" unless user && password

      define_method('basic_auth_params') do
        { user: user, password: password }
      end
    end

    # Pass path to YAML file containing Basic Auth parameters (i.e, both username and password)
    # @param [String] path_to_filename Will have Soaspec.credentials_folder appended to it if set
    def basic_auth_file(path_to_filename)
      basic_auth_cred = load_credentials_hash(path_to_filename)
      basic_auth(user: basic_auth_cred[:user], password: basic_auth_cred[:password])
    end

    # @param [Hash] headers Hash of REST headers used in RestClient
    def headers(headers)
      define_method('rest_client_headers') { headers }
    end

    # Convert each key from snake_case to PascalCase
    def pascal_keys(set)
      define_method('pascal_keys?') { set }
    end

    # Pass block to perform after every response is retrieved
    # @example Throw exception if response body has an 'error' element equal to true
    #   after_response do |response, handler|
    #     raise Soaspec::ResponseError if handler.value_from_path(response, 'error')
    #   end
    def after_response
      define_method('after_response') { |response, _self| yield response, self }
    end

    # @param [Array] exception_list List of exceptions to retry response on. Default is
    #   any REST exception
    # @param [Integer] pause Time to wait before retrying
    # @param [Integer] count Times to retry
    def retry_on_exceptions(exception_list = [RestClient::RestHandler], pause: 1,
                            count: 3)
      define_method('retry_on_exceptions') { exception_list }
      define_method('retry_pause_time') { pause }
      define_method('retry_exception_limit') { count }
    end

    private

    # Load credentials hash from a YAML using Soaspec.credentials_folder if set, adding '.yml' if not set
    # @return [Hash] Hash with credentials in it
    def load_credentials_hash(filename)
      raise ArgumentError, "Filename passed must be a String for #{self} but was #{filename.class}" unless filename.is_a?(String)

      full_path = Soaspec.credentials_folder ? File.join(Soaspec.credentials_folder, filename) : filename
      full_path += '.yml' unless full_path.end_with?('.yml') # Automatically add 'yml' extension
      raise "No file at #{full_path}. 'Soaspec.credentials_folder' is '#{Soaspec.credentials_folder}'" unless File.exist? full_path

      file_hash = YAML.load_file(full_path)
      raise "File at #{full_path} is not a hash" unless file_hash.is_a? Hash

      file_hash.transform_keys_to_symbols
    end
  end
end
