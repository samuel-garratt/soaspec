# frozen_string_literal: true

module Soaspec
  # Defaults for Soaspec RestParameters methods
  module RestParametersDefaults
    # This must be overridden by using 'base_url' method within class definition
    # @return [String] Set through following method. Base URL in REST requests.
    def base_url_value
      nil
    end

    # This returns the base url that can be accessed by a subclass. It is set by
    # the 'base_url' method
    #
    # @example
    #   class Parent < Soaspec::RestHandler
    #     base_url 'parent'
    #   end
    #
    #   class Child < Parent
    #     base_url "#{parent_url}/child_path"
    #   end
    #
    #   Child.new.base_url_value # => 'parent/child_path'
    # @return [String] Base url that can be accessed by a subclass.
    def parent_url
      raise 'This needs to be set through base_url method'
    end

    # @return [Hash] Headers used in RestClient
    def rest_client_headers
      {}
    end

    # Whether to convert each key in the request to PascalCase
    # It will also auto convert simple XPath, JSONPath where '//' or '..' not specified
    # @return [Boolean] Whether to convert to PascalCase
    def pascal_keys?
      false
    end
  end
end
