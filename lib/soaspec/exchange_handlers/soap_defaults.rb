# frozen_string_literal: true

module Soaspec
  # Default values set in order to make testing easier. Override to fit what you
  # need in testing
  module SoapDefaults
    class << self
      # Default Savon options. See http://savonrb.com/version2/globals.html for details
      # @example Things could go wrong if not set properly
      #   env_namespace: :soap, # Change environment namespace
      #   namespace_identifier: :tst, # Change namespace element
      #   element_form_default: :qualified # Populate each element with namespace
      #   namespace: 'http://Extended_namespace.xsd' change root namespace
      #   basic_auth: 'user', 'password'
      # @return [Hash] Default Savon options for all BasicSoapHandler
      def options
        {
          ssl_verify_mode: :none, # Easier for testing. Not so secure
          follow_redirects: true, # Necessary for many API calls
          soap_version: 2, # use SOAP 1.2. You will get 415 error if this is incorrect
          raise_errors: false # HTTP errors not cause failure as often negative test scenarios expect not 200 response
        }
      end
    end
  end
end
