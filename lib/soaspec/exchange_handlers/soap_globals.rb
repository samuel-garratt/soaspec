# frozen_string_literal: true

module Soaspec
  # Singleton methods for assigning Savon globals (http://savonrb.com/version2/globals.html)
  module SoapGlobals
    # Set SOAP WSDL. Use namespace and endpoint if no WSDL defined
    # @param [String] path Path to wsdl, either on network or locally
    def wsdl(path)
      define_method('wsdl') { path }
    end

    # Set Basic auth
    def basic_auth(user, password)
      define_method('basic_auth') { [user, password] }
    end

    # SOAP version number. Default is version 1.2
    # @example SOAP Version 1.1
    #   soap_version 1
    def soap_version(number)
      define_method('soap_version') { number }
    end

    %w[raise_errors namespace namespaces endpoint ssl_verify_mode].each do |method|
      define_method(method) do |param|
        define_method(method) { param }
      end
    end
  end
end
