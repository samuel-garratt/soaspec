# frozen_string_literal: true

require 'fileutils'
module Soaspec
  # Help with tasks common to soaspec executables
  module ExeHelpers
    # Create files in project depending on type of project
    # @param [String] type Type of project to create, e.g., 'soap', 'rest'
    def create_files_for(type)
      case type
      when 'soap'
        create_files %w[lib/blz_service.rb lib/shared_example.rb spec/soap_spec.rb]
        create_file(filename: 'template/soap_template.xml', erb: false)
      when 'rest'
        create_files %w[spec/rest_spec.rb lib/package_service.rb]
      else
        # TODO: This needs to have placeholders explaining what to fill in
      end
    end

    # @param [Array] filenames List of files to create
    def create_files(filenames, ignore_if_present: false)
      raise ArgumentError, 'Expected filenames to be an Array' unless filenames.is_a? Array

      filenames.each { |name| create_file filename: name, ignore_if_present: ignore_if_present }
    end

    # Spec task string depending upon whether virtual is used
    def spec_task
      task_name = options[:virtual] ? 'spec: :start_test_server' : ':spec'
      "RSpec::Core::RakeTask.new(#{task_name}) do |t|"
    end

    # Retrieve default file contents based on filename
    # @param [String] filename Filename within 'lib/generator' to file retrieve contents from
    # @param [Boolean] erb Whether to process file with ERB
    def retrieve_contents(filename, erb = true)
      default_file = if filename.start_with?('../')
                       File.join(File.dirname(__FILE__), filename[3..-1] + (erb ? '.erb' : ''))
                     else
                       File.join(File.dirname(__FILE__), 'generator', filename + (erb ? '.erb' : ''))
                     end
      contents = File.read(default_file)
      erb ? ERB.new(contents).result(binding) : contents
    end

    # @param [String] filename Name of the file to create
    # @param [String] content Content to place inside file
    # @param [Boolean] ignore_if_present Don't complain if file is present
    # @return [String] String describing if file created or not
    def create_file(filename: nil, content: nil, ignore_if_present: false, erb: true)
      raise 'Need to pass filename' unless filename

      content ||= retrieve_contents(filename, erb)
      create_folder File.split(filename).first
      msg = if File.exist? filename
              old_content = File.read(filename)
              if old_content != content && !ignore_if_present
                "!! #{filename} already exists and differs from template"
              else
                "#{filename} already exists"
              end
            else
              File.open(filename, 'w') { |f| f.puts content }
              "Created: #{filename}"
            end
      puts msg
      msg
    end

    # Create folder if there's not a file already there.
    # Will create parent folder if necessary.
    # @param [String] folder Folder to create
    def create_folder(folder)
      if File.exist? folder
        warn "!! #{folder} already exists and is not a directory" unless File.directory? folder
      else
        FileUtils.mkdir_p folder
        puts "Created folder: #{folder}/"
      end
    end

    # @return [String] Create class representing wsdl in general
    def class_content
      ERB.new(File.read(File.join(File.dirname(__FILE__), 'generator', 'lib/dynamic_class_content.rb.erb'))).result(binding)
    end

    # Create a spec for an WSDL operation
    # @param [String] operation Used in ERB to create a test for a WSDL operation
    def generated_soap_spec_for(operation)
      ERB.new(File.read(File.join(File.dirname(__FILE__), 'generator', 'spec/dynamic_soap_spec.rb.erb'))).result(binding)
    end
  end
end
