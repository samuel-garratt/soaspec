# Api to execute an API based on parameters
class ExecApi < Soaspec::RestHandler
  base_url ENV['base_url']
  if ENV['token_url']
    params = { token_url: ENV['token_url'], client_id: ENV['client_id'], client_secret: ENV['client_secret'] }
    params.merge!(username: ENV['username']) if ENV['username']
    params.merge!(password: ENV['password']) if ENV['password']
    oauth2 params
  end
end
