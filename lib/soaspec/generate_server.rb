# frozen_string_literal: true

require 'sinatra'

module Soaspec
  # Server for generating code
  class GenerateServer < Sinatra::Application
    include Soaspec::ExeHelpers

    class << self
      # @return [Hash] Parameters used when creating handler
      attr_accessor :create_params
    end

    get '/css/bootstrap.css' do
      [200, { 'Content-Type' => 'text/css' }, retrieve_contents('css/bootstrap.css', false)]
    end

    # Webpage user can interact with to generate an ExchangeHandler
    get '/' do
      GenerateServer.create_params ||= {}
      puts GenerateServer.create_params
      retrieve_contents('generate_exchange.html')
    end

    # When a post is made, generate the class file, save the parameters
    # and redirect back to generate page
    post '/generate' do
      type = :rest # SOAP will be added to later
      @name = params['className']
      @base_url = params['baseUrl']
      @basic_auth_user = params['basicAuthUser']
      @basic_auth_password = params['basicAuthPassword']
      unless @basic_auth_user&.empty?
        auth = "user: '#{@basic_auth_user}'
password: '#{@basic_auth_password}'"
        create_file filename: File.join(Soaspec.credentials_folder, "#{@name.snakecase}.yml"),
                    content: auth
      end
      filename = File.join('lib', "#{params['className'].snakecase}.rb")
      feedback = create_file filename: filename,
                             content: retrieve_contents(File.join('lib', "new_#{type}_service.rb"))
      GenerateServer.create_params = params
      GenerateServer.create_params['feedback'] = feedback
      redirect '/'
    end
  end
end
