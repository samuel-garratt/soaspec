# frozen_string_literal: true

require 'hashie'

# Hash that allows accessing hash with either string or Hash
class IndifferentHash < Hash
  include Hashie::Extensions::MergeInitializer
  include Hashie::Extensions::IndifferentAccess
end
