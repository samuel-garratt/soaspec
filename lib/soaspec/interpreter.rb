# frozen_string_literal: true

# Help interpret the general type of a particular object
class Interpreter
  class << self
    # @return [Error] XML Errors found in interpreting response
    attr_accessor :xml_errors

    # @return [Error] JSON Errors found in interpreting response
    attr_accessor :json_errors

    # @param [Object] response API response
    # @return [Symbol] Type of provided response
    def response_type_for(response)
      @xml_errors = nil
      @json_errors = nil
      @response = response
      case @response
      when String
        if xml?
          :xml
        elsif json?
          :json
        elsif html?
          :html
        else
          :string
        end
      when Hash then :hash
      when Nokogiri::XML::NodeSet, Nokogiri::XML::Document, Savon::Response then :xml
      else
        :unknown
      end
    end

    # @return [Boolean] Whether response has tag like syntax similar to XML. Could be a syntax error occurred
    def looks_like_xml?
      @response[0] == '<' && @response[-1] == '>'
    end

    # @return [Boolean] Whether response has bracket like syntax similar to JSON. Could be a syntax error occurred
    def looks_like_json?
      @response[0] == '{' && @response[-1] == '}'
    end

    # @return [String] Description of error
    def diagnose_error
      return xml_errors if looks_like_xml?

      return json_errors if looks_like_json?

      ''
    end

    # @return [Boolean] Whether valid XML
    def xml?
      Nokogiri::XML(@response) { |config| config.options = Nokogiri::XML::ParseOptions::STRICT }
      true
    rescue Nokogiri::XML::SyntaxError => e
      self.xml_errors = e
      false
    end

    # @return [Boolean] Whether valid HTML. Must include at least one tag
    def html?
      Nokogiri::HTML(@response) do |config|
        config.options = Nokogiri::XML::ParseOptions::DEFAULT_HTML
      end
      @response.include?('<') && @response.include?('>')
    rescue Nokogiri::XML::SyntaxError => e
      self.xml_errors = e
      false
    end

    # @return [Boolean] Whether valid JSON
    def json?
      JSON.parse(@response)
    rescue JSON::ParserError => e
      self.json_errors = e
      false
    end
  end
end
