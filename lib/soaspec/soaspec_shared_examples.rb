# frozen_string_literal: true

require 'rspec'

RSpec.shared_examples_for 'success scenario' do
  it 'has successful status code' do
    expect(described_class.successful_status_code?).to be true
  end
  context 'has expected mandatory elements' do
    described_class.exchange_handler.expected_mandatory_elements.each do |mandatory_element|
      it mandatory_element do
        expect(described_class).to contain_key mandatory_element
      end
    end
  end
  described_class.exchange_handler.expected_mandatory_xpath_values.each do |xpath, value|
    it "has xpath '#{xpath}' equal to '#{value}'" do
      expect(described_class).to have_xpath_value(xpath => value)
    end
  end
  described_class.exchange_handler.expected_mandatory_json_values.each do |xpath, value|
    it "has json '#{xpath}' equal to '#{value}'" do
      expect(described_class).to have_xpath_value(xpath => value)
    end
  end
end
