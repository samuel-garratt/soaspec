# frozen_string_literal: true

require 'logger'
require 'fileutils'
require 'colorize'

module Soaspec
  # Custom class for logging API traffic
  class ApiLogger < Logger
    #
    # Rewrite << to use info logger with formatting
    #
    def <<(msg)
      info msg
    end
  end

  # Handles logs of API requests and responses
  class SpecLogger
    # Folder to put API traffic logs
    @traffic_folder = 'logs'
    # Whether to output api traffic to terminal
    @output_to_terminal = false
    # Color shown when displaying in terminal
    @terminal_color = :light_blue
    # Whether to output API traffic to log file
    @output_to_file = true
    # Time test run. Will only be calculated once once called
    @time_test_run = Time.now.strftime('%Y-%m-%d_%H_%M_%S')
    # By default file is based on time
    @traffic_file = nil
    # Name of program to include in logs
    @progname
    class << self
      # Folder to put API traffic logs
      attr_accessor :traffic_folder
      # Color shown when displaying in terminal
      attr_accessor :terminal_color
      # Readers for log parameters
      attr_reader :output_to_terminal, :output_to_file, :time_test_run
      # @return [String] Name of program to include in logs
      attr_accessor :progname
      # @return [String] String representing date format to use for traffic
      attr_writer :time_format
      # Logger used to log API requests
      attr_accessor :logger

      # String representing date format to use for traffic
      def time_format
        @time_format || '%H:%M:%S'
      end

      # Set file to log traffic to
      # @param [String] file Path to traffic file to use
      def traffic_file=(file)
        @traffic_file = file
        reset_log
      end

      # @return [String] Traffic file to create logs at
      def traffic_file
        return File.join(traffic_folder, @traffic_file) if @traffic_file

        filename = "traffic_#{time_test_run}.log"
        File.join(traffic_folder, filename)
      end

      # Unset Logger object. It will be recreated on next time 'info' is called
      def reset_log
        @logger = nil
        RestClient.log = nil
      end

      # Whether to log all API traffic
      def log_api_traffic=(set)
        @log_api_traffic = set
        reset_log
      end

      # @return [Boolean] Whether to log all API traffic
      def log_api_traffic?
        @log_api_traffic.nil? ? true : @log_api_traffic
      end

      # @param [Symbol, Boolean] value # Whether to output API traffic to log file
      def output_to_file=(value)
        @output_to_file = value
        reset_log
      end

      # @param [Symbol, Boolean] value # Whether to output API traffic to STDOUT
      def output_to_terminal=(value)
        @output_to_terminal = value
        reset_log
      end

      # Create new log file if necessary and setup logging level
      # @return [Logger] Logger class to record API traffic
      def create
        create_log_file
        @logger = ApiLogger.new(traffic_file) # Where request and responses of APIs are stored
        @logger.progname = progname || 'SpecLog'
        @logger.formatter = proc do |_severity, datetime, progname, msg|
          message = "#{progname}, [#{datetime.strftime(time_format)}] : #{msg}\n"
          print message.colorize(terminal_color) if @output_to_terminal
          message if @output_to_file
        end
        RestClient.log = @logger
        @logger
      end

      # Log a message using Soaspec logger
      # Logger (and it's file) will be created if it's not already set
      # @param [String, Array] message The message to add to the logger or list of messages
      def info(message)
        return unless log_api_traffic?

        create unless @logger
        if message.respond_to? :each
          message.each do |message_item|
            info(message_item)
          end
        else
          if block_given?
            @logger.info(message) { yield }
          else
            @logger.info(message)
          end
        end
      end

      private

      # Create folder and file to store logs
      def create_log_file
        return if File.exist?(traffic_file)

        FileUtils.mkdir_p traffic_folder
        FileUtils.touch traffic_file
      end
    end
  end
end
