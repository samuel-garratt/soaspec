# frozen_string_literal: true

require 'erb'

module Soaspec
  # Handles reading templates for tests
  class TemplateReader
    # Name of file where template is stored
    attr_accessor :template_name

    # @return [String] Path to where template file is stored
    def file_location
      File.join(*Soaspec.template_folder, template_name)
    end

    # @param [String] template_name File where template is stored
    # @return [String] Body of template after determining test_values
    def render_body(template_name, test_values)
      test_values = IndifferentHash.new(test_values) # Allow test_values to be either Symbol or String
      test_values&.each do |key, value|
        instance_variable_set("@#{key}", value)
      end
      self.template_name = template_name
      unless File.exist? file_location
        raise "Cannot see file at #{file_location}. "\
              "Global folder is '#{Soaspec.template_folder}' and filename is '#{template_name}'"
      end
      request_body = File.read file_location
      raise "Template at #{file_location} not parsed correctly" if request_body.strip.empty?

      ERB.new(request_body).result(binding)
    end
  end
end
