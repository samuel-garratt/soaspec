# frozen_string_literal: true

require 'erb'

module Soaspec
  module TestServer
    # Representing a GetBank SOAP service
    class GetBank
      class << self
        # This is retrieved by Savon
        # @return [String] WSDL of mock web service
        def test_wsdl
          ERB.new(File.read(File.join(File.dirname(__FILE__), 'bank.wsdl'))).result(binding)
        end

        # @return [String] XML of success SOAP Response
        def success_response_template
          <<-EOF
    <soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
      <soapenv:Body>
        <ns1:getBankResponse xmlns:ns1="http://thomas-bayer.com/blz/">
          <ns1:details unique_id="50">
            <ns1:bezeichnung lang="German"><%= @title %></ns1:bezeichnung>
            <ns1:bic>DEUTDEMMXXX <%= soap_action %></ns1:bic>
            <ns1:ort>München</ns1:ort>
            <% if @multiple_ort %>
            <ns1:ort>Wellington</ns1:ort>
            <ns1:ort>Tokyo</ns1:ort>
            <% end %>
            <ns1:plz><%= @bank_name %></ns1:plz>
          </ns1:details>
        </ns1:getBankResponse>
      </soapenv:Body>
    </soapenv:Envelope>
          EOF
        end

        # @return [String] XML of failure SOAP Response simulating bank not found
        def bank_not_found
          <<-EOF
        <soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
        <soapenv:Body>
        <soapenv:Fault>
        <soapenv:Code>
        <soapenv:Value>soapenv:Receiver</soapenv:Value>
          </soapenv:Code>
        <soapenv:Reason>
        <soapenv:Text xml:lang="en-US">Keine Bank zur BLZ <%= @bank_name %> gefunden!</soapenv:Text>
          </soapenv:Reason>
        <soapenv:Detail>
        <Exception>org.apache.axis2.AxisFault: Keine Bank zur BLZ test string gefunden!
        at com.thomas_bayer.blz.BLZService.getBank(BLZService.java:41)
        at com.thomas_bayer.blz.BLZServiceMessageReceiverInOut.invokeBusinessLogic(BLZServiceMessageReceiverInOut.java:49)
        at org.apache.axis2.receivers.AbstractInOutSyncMessageReceiver.invokeBusinessLogic(AbstractInOutSyncMessageReceiver.java:42)
        at org.apache.axis2.receivers.AbstractMessageReceiver.receive(AbstractMessageReceiver.java:96)
        at org.apache.axis2.engine.AxisEngine.receive(AxisEngine.java:145)
        at org.apache.axis2.transport.http.HTTPTransportUtils.processHTTPPostRequest(HTTPTransportUtils.java:275)
        at org.apache.axis2.transport.http.AxisServlet.doPost(AxisServlet.java:120)
        at javax.servlet.http.HttpServlet.service(HttpServlet.java:646)
        at javax.servlet.http.HttpServlet.service(HttpServlet.java:727)
        at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:303)
        at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:208)
        at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:220)
        at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:122)
        at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:501)
        at org.apache.catalina.valves.AccessLogValve.invoke(AccessLogValve.java:950)
        at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:170)
        at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:98)
        at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:116)
        at org.apache.catalina.valves.RemoteIpValve.invoke(RemoteIpValve.java:683)
        at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:408)
        at org.apache.coyote.http11.AbstractHttp11Processor.process(AbstractHttp11Processor.java:1041)
        at org.apache.coyote.AbstractProtocol$AbstractConnectionHandler.process(AbstractProtocol.java:607)
        at org.apache.tomcat.util.net.JIoEndpoint$SocketProcessor.run(JIoEndpoint.java:315)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
        at java.lang.Thread.run(Thread.java:745)
        </Exception>
          </soapenv:Detail>
        </soapenv:Fault>
      </soapenv:Body>
        </soapenv:Envelope>
          EOF
        end

        def error_response_template
          <<-EOF
    <soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
      <soapenv:Body>
        <soapenv:Fault>
          <soapenv:Code>
            <soapenv:Value>soapenv:Receiver</soapenv:Value>
          </soapenv:Code>
          <soapenv:Reason>
            <soapenv:Text xml:lang="en-US">org.apache.axis2.databinding.ADBException: Unexpected subelement getBank</soapenv:Text>
          </soapenv:Reason>
          <soapenv:Detail>
            <Exception>org.apache.axis2.AxisFault: org.apache.axis2.databinding.ADBException: Unexpected subelement getBank
      at org.apache.axis2.AxisFault.makeFault(AxisFault.java:417)
      at com.thomas_bayer.blz.BLZServiceMessageReceiverInOut.fromOM(BLZServiceMessageReceiverInOut.java:124)
      at com.thomas_bayer.blz.BLZServiceMessageReceiverInOut.invokeBusinessLogic(BLZServiceMessageReceiverInOut.java:43)
      at org.apache.axis2.receivers.AbstractInOutSyncMessageReceiver.invokeBusinessLogic(AbstractInOutSyncMessageReceiver.java:42)
      at org.apache.axis2.receivers.AbstractMessageReceiver.receive(AbstractMessageReceiver.java:96)
      at org.apache.axis2.engine.AxisEngine.receive(AxisEngine.java:145)
      at org.apache.axis2.transport.http.HTTPTransportUtils.processHTTPPostRequest(HTTPTransportUtils.java:275)
      at org.apache.axis2.transport.http.AxisServlet.doPost(AxisServlet.java:120)
      at javax.servlet.http.HttpServlet.service(HttpServlet.java:646)
      at javax.servlet.http.HttpServlet.service(HttpServlet.java:727)
      at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:303)
      at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:208)
      at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:220)
      at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:122)
      at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:501)
      at org.apache.catalina.valves.AccessLogValve.invoke(AccessLogValve.java:950)
      at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:170)
      at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:98)
      at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:116)
      at org.apache.catalina.valves.RemoteIpValve.invoke(RemoteIpValve.java:683)
      at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:408)
      at org.apache.coyote.http11.AbstractHttp11Processor.process(AbstractHttp11Processor.java:1041)
      at org.apache.coyote.AbstractProtocol$AbstractConnectionHandler.process(AbstractProtocol.java:607)
      at org.apache.tomcat.util.net.JIoEndpoint$SocketProcessor.run(JIoEndpoint.java:315)
      at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
      at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
      at java.lang.Thread.run(Thread.java:745)
    Caused by: java.lang.Exception: org.apache.axis2.databinding.ADBException: Unexpected subelement getBank
      at com.thomas_bayer.adb.GetBankType$Factory.parse(GetBankType.java:423)
      at com.thomas_bayer.adb.GetBank$Factory.parse(GetBank.java:304)
      at com.thomas_bayer.blz.BLZServiceMessageReceiverInOut.fromOM(BLZServiceMessageReceiverInOut.java:117)
      ... 25 more
    Caused by: org.apache.axis2.databinding.ADBException: Unexpected subelement getBank
      at com.thomas_bayer.adb.GetBankType$Factory.parse(GetBankType.java:410)
      ... 27 more
    </Exception>
          </soapenv:Detail>
        </soapenv:Fault>
      </soapenv:Body>
    </soapenv:Envelope>
          EOF
        end

        # Return a response based upon the SOAP request
        # @param [String] request XML in request
        def response_for(request)
          @title = 'Deutsche Bank'
          soap_action = request.env['HTTP_SOAPACTION']
          return 500, 'Not valid SOAP' unless soap_action

          request_body = request.body
          doc = Nokogiri::XML(request_body)
          soap_action = soap_action.strip # Used in ERB
          blz_element = doc.at_xpath('//tns:blz')
          return 500, error_response_template unless blz_element

          @bank_name = blz_element.inner_text
          @bank_id = @bank_name.to_i
          return 500, ERB.new(bank_not_found).result(binding) if @bank_id.zero?

          @title = 'DAB Bank' if @bank_id == 500
          @multiple_ort = (@bank_id == 20)
          ERB.new(success_response_template).result(binding)
        end
      end
    end
  end
end
