# frozen_string_literal: true

module Soaspec
  module TestServer
    # Handles returning ids dependent on a number of factors
    # Made to demonstrate creating testing a list of ids where a few return false
    # when not developed and checking each value in the list can find the false value
    class IdManager
      # @return [Boolean] Whether to simulate a fully developed state where everything is done
      @developed = false
      class << self
        attr_accessor :developed

        # Once developed is set, everything will return true. A 'false' value represents something
        # not developed
        # @param [String] num Number representing tester or test number
        # @param [String] id Id used in test
        # @return  Result depending on num, id and whether '@developed' is set
        def result_for(num, id)
          result = undeveloped_id(num, id) unless @developed
          result || 'true'
        end

        # Idea is that this will return false for a particular id for different test numbers
        # @param [String] num Number representing tester or test number
        # @param [String] id Id used in test
        # @return [String] true of false depending on num and id
        def undeveloped_id(num, id)
          case num
          when '1', '4', '10' then 'false' if id == '40'
          when '3', '11', '9' then 'false' if id == '74'
          when '2', '6', '8' then 'false' if id == '80'
          when '7', '5', '12' then 'false' if id == '64'
          else
            'true'
          end
        end
      end
    end
  end
end
