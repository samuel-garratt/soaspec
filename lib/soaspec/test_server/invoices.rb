# frozen_string_literal: true

module Soaspec
  module TestServer
    # Used to simulate requests requiring oauth authentication
    class Invoices
      @user_used = nil

      class << self
        attr_accessor :user_used

        def oauth_headers
          {
            'Content-Type' => 'application/json;charset=UTF-8'
          }
        end

        def oauth_body
          {
            access_token: 'TEST_TOKENiIsImtpZCI6IlRFU1QifQ.AAAABBBBRfaWQiOiJhYWQ5MjY3SIMULATE_LARGE_TOKEN3MmM5OGQ5NGE2YTU5YSIsImV4cCI6MTUyNzU3MTY4Mywic2NvcGUiOltdfQ.3OmCdW7fLZMUST_BE_ABLE_TO_HANDLEgAGaJB0lFYyhaw',
            token_type: 'Bearer',
            expires_in: '86399',
            instance_url: %w[http://this_is_my_instance http://this_is_second_instance].sample
          }
        end
      end
    end
  end
end
