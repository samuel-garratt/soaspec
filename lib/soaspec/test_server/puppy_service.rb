# frozen_string_literal: true

module Soaspec
  module TestServer
    # Simulates ordering a new puppy. Used for testing REST storing, retrieving and updating data
    class PuppyService
      @data = {}
      @current_id = 1
      class << self
        attr_accessor :data

        def new_id
          @data[@current_id] = {}
          @data[@current_id][:Id] = @current_id
          @current_id += 1
          @current_id - 1
        end
      end
    end
  end
end
