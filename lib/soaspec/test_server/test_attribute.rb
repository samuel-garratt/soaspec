# frozen_string_literal: true

module Soaspec
  module TestServer
    # Helps tests attribute methods
    class TestAttribute
      class << self
        def note
          File.read(File.join(File.dirname(__FILE__), 'note.xml'))
        end
      end
    end
  end
end
