# frozen_string_literal: true

module Soaspec
  module TestServer
    # Helps tests attribute methods
    class TestNamespace
      class << self
        def food
          File.read(File.join(File.dirname(__FILE__), 'namespace.xml'))
        end
      end
    end
  end
end
