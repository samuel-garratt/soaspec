# frozen_string_literal: true

module Soaspec
  # @return [String] Version of the gem
  VERSION = '0.4.0'
end
