# frozen_string_literal: true

module Soaspec
  class TimeOutError < StandardError; end
  # Class to enable waiting for an expected condition to return true
  class Wait
    DEFAULT_TIMEOUT  = 5
    DEFAULT_INTERVAL = 0.2

    #
    # Wait until the given block returns a true value.
    #
    # @param [Hash] opts Options for this instance
    # @option opts [Numeric] :timeout (5) Seconds to wait before timing out.
    # @option opts [Numeric] :interval (0.2) Seconds to sleep between polls.
    # @option opts [String] :message Exception mesage if timed out.
    # @option opts [Array, Exception] :ignore Exceptions to ignore while polling (default: Error::NoSuchElementError)
    # @raise [Error::TimeOutError]
    # @return [Object] the result of the block
    #
    def self.until(opts = {})
      timeout = opts.fetch(:timeout, DEFAULT_TIMEOUT)
      ignored = Array(opts[:ignore] || NoElementAtPath)
      interval = opts.fetch(:interval, DEFAULT_INTERVAL)
      end_time = Time.now + timeout
      last_error = nil

      until Time.now > end_time
        begin
          result = yield
          return result if result
        rescue *ignored => e
          # swallowed
        end
        sleep interval
      end

      msg = opts[:message] ? opts[:message].dup : "timed out after #{timeout} seconds with interval of #{interval}"
      msg << " (#{last_error.message})" if last_error
      raise Soaspec::TimeOutError, msg
    end
  end
end
