# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'soaspec/version'

Gem::Specification.new do |spec|
  spec.name          = 'soaspec'
  spec.version       = Soaspec::VERSION
  spec.authors       = ['SamuelGarrattIQA']
  spec.email         = ['samuel.garratt@integrationqa.com']

  spec.summary       = "Helps to create tests for 'SOAP' or 'REST' apis "
  spec.description   = "Helps to create tests for 'SOAP' or 'REST' apis. Easily represent multiple requests with
the same configuration. Examples designed for RSpec and Cucumber."
  spec.homepage      = 'https://gitlab.com/samuel-garratt/soaspec'
  spec.required_ruby_version = Gem::Requirement.new(">= 2.5")
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features|bin|config|templates|credentials|images|demo|support)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'cucumber', '< 4.0'
  spec.add_development_dependency 'data_magic'
  spec.add_development_dependency 'factory_bot'
  spec.add_development_dependency 'parallel_tests'
  spec.add_development_dependency 'rack'
  spec.add_development_dependency 'rack-test'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'yard-cucumber'
  spec.add_development_dependency 'yard-doctest'
  spec.add_dependency 'activesupport'
  spec.add_dependency 'colorize'
  spec.add_dependency 'faker'
  spec.add_dependency 'hashie'
  spec.add_dependency 'jsonpath'
  spec.add_dependency 'launchy'
  spec.add_dependency 'nokogiri'
  spec.add_dependency 'pry'
  spec.add_dependency 'rackup'
  spec.add_dependency 'random-port'
  spec.add_dependency 'require_all', '>= 1.5.0'
  spec.add_dependency 'rest-client', '>= 2.0' # REST
  spec.add_dependency 'rspec', '~> 3.0' # This framework is designed to work with RSpec
  spec.add_dependency 'rspec-its'
  spec.add_dependency 'savon', '>= 2' # SOAP
  spec.add_dependency 'sinatra'
  spec.add_dependency 'sinatra-basic-auth'
  spec.add_dependency 'sinatra-docdsl'
  spec.add_dependency 'thor'
  spec.add_dependency 'xml-simple', '>= 1.1.5'
end
