In here is the unit and integration tests for `soaspec`.

# Sections

The main objective of `soaspec` is to main building API requests (`building_request`)
and extracting/verifying their response (`extracting_response`) easy.
There are specific sections for these 2 sections.

Other sections include:

* exe - Has tests verifying the `soaspec` executable that gets installed by this gem
* unit - Has tests for units within `soaspec` that are usually used behind the scenes when working with
         `soaspec`
* support - Has common classes and shared examples used by the tests
* virtual_server - Has tests for the virtual server that hosts a sinatra API used by `soaspec`s tests and
                   can be used for learning API testing
* integration - Examples of using all of soaspec features to test various scenarios