# frozen_string_literal: true

# This is for REST. See Blz_soap_service for example of basic auth in SOAP
include Soaspec::RestMethods # Defines Rest methods globally
# Soaspec.credentials_folder = 'credentials'

class BasicAuthSvc < Soaspec::RestHandler
  # basic_auth user: 'admin', password: 'secret' If you want to insert data options directly
  basic_auth_file 'basic_auth' # Load YAML file with basic auth credentials in it
  base_url 'http://localhost:4999/basic_secrets' # Location that requires basic auth to access
end

context BasicAuthSvc.new('Basic Auth') do
  describe get(:basic_auth) do
    it 'request params set correctly' do
      expect(subject.request_parameters.basic_auth_user).to eq 'admin'
      expect(subject.request_parameters.basic_auth_password).to eq 'secret'
    end
    it { is_expected.to include_in_body 'Secret data' }
    it 'has correct basic auth params' do
      expect(subject.request.user).to eq 'admin'
      expect(subject.request.password).to eq 'secret'
    end
  end
end
