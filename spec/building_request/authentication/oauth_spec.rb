# frozen_string_literal: true

Soaspec.credentials_folder = 'credentials'
# This will put password in log file, only recommended for debugging.
# See unit tests for examples of setting different options
Soaspec::OAuth2.debug_oauth = true
include Soaspec::RestMethods # Defines Rest methods globally
require 'support/oauth_service'

class CurrentUser
  class << self
    attr_accessor :name
  end
end

class SecretExchange < Exchange
  default_handler OAuthSvc, 'Get Invoice', api_username: '<%= CurrentUser.name %>' # 'user 2'
end

oauth = OAuthSvc.new('Get Invoice')
test_id = '77700007'

context oauth do
  describe get(:oauth, suburl: test_id) do
    it 'uses authorisation header' do
      puts subject.request_parameters
      expect(subject.request_parameters.headers[:authorization]).to start_with 'Bearer'
    end
    it_behaves_like 'success scenario'
    its(:customer_id) { is_expected.to eq test_id }
    its([:oauth]) { is_expected.to include 'TEST_TOKEN' } # Token should be echoed back
    its([:user]) { is_expected.to eq 'Fixed user' }
  end
end
describe 'Changing oauth user' do
  it 'new user is used' do
    CurrentUser.name = 'user 2'
    exchange = SecretExchange.new('New User', method: :get, suburl: test_id)
    expect(exchange['user']).to eq 'user 2'
  end
  it 'uses a third user' do
    CurrentUser.name = 'user 3'
    exchange = SecretExchange.new('New User', method: :get, suburl: test_id)
    expect(exchange['user']).to eq 'user 3'
  end
end
