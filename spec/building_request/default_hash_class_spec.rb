# frozen_string_literal: true

# Example of a RestHandler that sets a default hash on the class definition of an ExchangeHandler
class DefaultHashSet < Soaspec::RestHandler
  base_url "#{VIRTUAL_SERVER_LOCATION}/echoer"

  # Example of setting 'default_hash' for request body within class.
  # Note this will be converted to JSON
  default_hash test: { name: { first: 'bob', last: 'smith' } }
end

describe DefaultHashSet.post(name: 'Using default hash') do
  it 'Sends default request if no body or payload set' do
    handler = subject.exchange_handler
    expect(handler.respond_to?(:default_hash_value)).to be true
    expect(handler.instance_variable_get(:@default_hash)).to eq(test: { name: { first: 'bob', last: 'smith' } })
    expect(subject.to_hash).to eq('test' => { 'name' => { 'first' => 'bob', 'last' => 'smith' } })
  end
end

describe DefaultHashSet.post(name: 'Overriding default', body: { test: 5 }) do
  it 'receives overridden values' do
    expect(subject.request_parameters[:body]).to eq('{"test":5}')
    expect(subject.request_parameters.method).to eq(:post)
    expect(subject.to_hash).to eq('test' => 5)
  end
end
