# frozen_string_literal: true

require 'support/blz_soap_service'
# This example sets a default hash on the exchange handler's instance and then demonstrates how it
# can be overridden within specific exchanges

id = '70070010'

# TODO: Add test to check camel case conversion
# TODO: Extracting from response should be in separate spec
# TODO: Storing and retrieving should be in separate spec
# Either hash representation should be possible for CamelCase case due to 'convert_request_keys_to: :camelcase' option

context 'BasicSoapHandler', '#default_hash' do
  context BLZService.new(operation: :get_bank, default_hash: { blz: id }) do
    describe Exchange.new(:default) do
      it 'has expected hash' do
        expect(subject.request_parameters[:body][:message]).to eq(blz: id)
      end
      it { is_expected.to contain_value id }
      it { is_expected.to include_in_body id }
      its(:to_hash) { is_expected.to be_instance_of Hash }
      it_behaves_like 'success scenario'
      after(:all) { described_class.store(:title, 'bezeichnung') } # Value is stored here (used in next describe block)
    end

    describe Exchange.new(:xpath_eg, blz: 100_000) do
      its(['plz']) { is_expected.to eq '100000' }
      it { is_expected.to have_xpath_value '//ns1:bezeichnung' => 'Deutsche Bank' }
      context 'Handle retrieving stored value' do
        it { is_expected.to have_xpath_value 'bezeichnung' => described_class.retrieve(:title) } # Value stored retrieved here
      end
    end

    describe Exchange.new(:yaml_eg, data_for(:small_id)) do
      it_behaves_like 'success scenario'
    end

    # Retry for success more for web services that intermittently fail
    describe Exchange.new(:short_hand_xpath).retry_for_success do
      # Be careful. If you call a method that does not use namespaces, calling one that does may not find the element
      its(['ns1:bezeichnung']) { is_expected.to eq 'Deutsche Bank' } # '//' is not required at the beginning
    end
    describe Exchange.new('Check existence of elements') do
      it { is_expected.to have_element_at_xpath '//ns1:bezeichnung' }
      it { is_expected.not_to have_element_at_xpath '//ns1:bezeichnung_pretend' }
    end
  end
end
