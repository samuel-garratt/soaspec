# frozen_string_literal: true

# This code is for verifying usage with the factory_bot gem
require 'support/puppy_service'

id = '15435006001660'
cat_id = '2432432'
parent_id = '5239423'

class PuppyPlace < Exchange
  @exchange_handler = PuppyService.new('Factory')
end

class ParentPuppy < Exchange
  @exchange_handler = PuppyService.new('Parent')
end

FactoryBot.define do
  factory :parent_puppy do
    test_name { 'Parent puppy' }
    id { parent_id }
    name { 'factory pup' }
  end

  factory :puppy_place do
    test_name { 'Factory puppy' }
    id { id }
    category { { id: cat_id, name: 'category_name' } }
    association :name, factory: :parent_puppy # Unrealistic but tests functionality nonetheless
    photoUrls { [- 'test URL'] }
    tags { [{ id: 5, name: 'fact tag' }] }
    status { 'sold' }
  end
end

describe FactoryBot.build(:puppy_place) do
  its(:id) { is_expected.to eq id.to_i }
  its([:name]) { is_expected.to eq parent_id }
  its(['category.id']) { is_expected.to eq cat_id.to_i }
end
