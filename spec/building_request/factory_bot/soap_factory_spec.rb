# frozen_string_literal: true

require 'support/blz_soap_service'

final_id = '10060010'
pre_exchange_blz = '9234324'

@handler = BLZService.new('Factory bot', operation: :get_bank)

class PreExchange < Exchange
  default_handler BLZService, 'Pre Exchange', operation: :get_bank
end

class BankExchange < Exchange
  @exchange_handler = @handler
end

FactoryBot.define do
  factory :pre_exchange do
    blz { pre_exchange_blz }
    test_name { 'Pre exchange' }
  end

  factory :bank_exchange do
    test_name { 'Factory bank' }
    blz { final_id }
    pre_exchange

    trait :failure do
      test_name { 'Factory failure' }
      blz { 'Cause a failure' }
      fail_factory { true }
    end
  end
end

describe FactoryBot.create(:bank_exchange, :failure) do
  its(:status_code) { is_expected.to eq 500 }
end

describe 'Create exchange' do
  describe FactoryBot.create(:bank_exchange) do
    its([:plz]) { is_expected.to eq final_id }
    context 'pre-exchange' do
      it 'sets blz which is returned in plz' do
        expect(subject.pre_exchange['plz']).to eq pre_exchange_blz
      end
    end
  end

  it 'within block' do
    FactoryBot.create(:pre_exchange, blz: 43_534) do |exchange|
      expect(exchange.status_code).to eq 200
      expect(exchange[:plz]).to eq '43534'
    end
  end
end
