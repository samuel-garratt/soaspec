# frozen_string_literal: true

# Example of a class that inherits from a class that inherits from a ExchangeHandler class
require 'support/echo_service'

# This class inherits from PuppyService which has common information for ordering a puppy
# It then defines mandatory elements that should be present
class EchoServiceChild < EchoService
  base_url "#{parent_url}/extra_url"
end

describe EchoServiceChild.new('Grandchildren of Handler') do
  it 'takes both parent and child information' do
    expect(subject.base_url_value).to eq "#{VIRTUAL_SERVER_LOCATION}/echoer/extra_url"
  end
end
