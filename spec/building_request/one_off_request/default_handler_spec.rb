# frozen_string_literal: true

# This demonstrates how the default handler for a custom Exchange class can be set

require 'support/echo_service'

class CustomExchange < Exchange
  default_handler EchoService
end

describe 'Exchange with default handler' do
  subject(:exchange) { CustomExchange.new('Use Default', method: :post) }
  it 'uses default handler' do
    expect(exchange.exchange_handler).to be_instance_of EchoService
  end
  it 'can make request' do
    exchange.test_key = 'test_value'
    expect(exchange.response.body).to eq '{"test_key":"test_value"}'
  end
end
