# frozen_string_literal: true

# Demonstrating short hand, one off REST tests that modify an Exchange object when it's built

class TestService < Soaspec::RestHandler
  # Example of using a variable that will be interpreted via ERB when REST request made
  def server_name
    'localhost'
  end

  base_url '<%= server_name %>:4999/test/puppy'

  element :id, :id

  pascal_keys true
end

describe 'Correct data retrieved' do
  let(:test_name) { 'Dummy name' }
  it 'Post data and retrieve it' do
    id = TestService.post(body: { name: test_name }).id
    expect(TestService.get(suburl: id)[:name]).to eq test_name
  end
  it 'Pascal Handle unconventional key' do
    id = TestService.post(body: { name: test_name, Failure_Type__c: 'unconvential key' }).id
    expect(TestService.get(suburl: id)[:Failure_Type__c]).to eq 'unconvential key'
  end
end

describe 'Building Exchange step by step' do
  let(:sub_url_name) { 'stepper url' }
  let(:method_name) { 'stepper method' }
  it '.suburl = (value)' do
    id = TestService.post(body: { name: sub_url_name }).id

    stepper = Exchange.new('Stepper', method: :get)
    stepper.exchange_handler = TestService.new
    stepper.suburl = id
    expect(stepper[:name]).to eq sub_url_name
  end

  it '.method = (value)' do
    id = TestService.post(body: { name: 'old name' })[:id]

    stepper = Exchange.new('Stepper', suburl: id, body: { name: method_name })
    stepper.exchange_handler = TestService.new
    stepper.method = :patch
    expect(stepper[:with]).to eq method_name
  end
end
