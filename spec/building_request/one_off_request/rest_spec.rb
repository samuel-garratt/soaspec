# frozen_string_literal: true

# Demonstrating short hand, one off REST tests
require 'support/echo_service'
require 'support/puppy_service'

test_data = data_for('default/pet')

context 'Simple test scenarios' do
  describe EchoService.post('{ "test_key": "value" }') do
    it 'creates post exchange' do
      expect(subject).to be_instance_of Exchange
      expect(subject.request_parameters.method).to eq :post
      expect(subject.request_parameters.body).to eq('{ "test_key": "value" }')
    end
  end
  describe EchoService.get(suburl: 'extension') do
    it 'creates get exchange' do
      expect(subject).to be_instance_of Exchange
      expect(subject.request_parameters.method).to eq :get
      expect(subject.request_parameters.suburl).to eq 'extension'
    end
  end
  describe 'using block' do
    it 'passes exchange' do
      PuppyService.get(suburl: test_data['id']) do |exchange|
        expect(exchange).to be_instance_of Exchange
        expect(exchange).to be_successful
        expect(exchange['status']).to eq 'sold'
      end
    end
  end
  describe PuppyService.get(test_data['id']) do
    it 'sends suburl' do
      subject.call # To check 'request' the call has to be made
      expect(subject.request.url).to end_with test_data['id']
    end
  end
end

# Service to represent calls to root of Virtual Service
class BaseService < Soaspec::RestHandler
  base_url VIRTUAL_SERVER_LOCATION
end

# The benefit of this would be when sending requests to different
# endpoints via a series of loops
describe 'suburl as array' do
  let(:joined_path) { 'this/is/a/long/path' }
  it 'joins elements via /' do
    suburl_paths = %i[this is a long path]
    exchange = BaseService.get(suburl: suburl_paths)
    expect(exchange.request_parameters.suburl).to eq joined_path
    exchange.call
    expect(exchange.request.url).to end_with joined_path
  end
end
