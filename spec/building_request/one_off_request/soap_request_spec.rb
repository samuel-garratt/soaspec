# frozen_string_literal: true

# Example of building up one off SOAP requests
require 'support/blz_soap_service'
id = '70060010'

describe 'Building Exchange request via steps' do
  it 'creates request with steps set' do
    BLZService.new('Building Exchange via steps', operation: :get_bank).use
    stepper = Exchange.new('Stepper')
    stepper.blz = id # Set id in request. Separate step than creating exchange
    expect(stepper.element?(:plz)).to eq true
    expect(stepper[:plz]).to eq id # Returned in response in 'plz'
  end
end

# Not using within context block
BLZService.new(operation: :get_bank, default_hash: { blz: id }).use
describe Exchange.new(:not_within_context) do
  it_behaves_like 'success scenario'
end
# Example of creating 'Exchange' via SOAP operation
context 'create exchage via SOAP operation' do
  context 'describe block' do
    describe BLZService.get_bank(blz: id) do
      it_behaves_like 'success scenario'
    end
  end
  it 'operate within own block' do
    BLZService.get_bank(blz: id) do |bank_exchange|
      expect(bank_exchange).to be_successful
      expect(bank_exchange.element?(:plz)).to eq true
    end
  end
end

describe '.operations' do
  it 'shows operations list' do
    expect(BLZService.operations).to eq [:get_bank]
  end
end
