# frozen_string_literal: true

require 'support/echo_service'

RSpec.describe 'Rare REST' do
  it 'send Post with empty body' do
    expect(EchoService.post('').response.body).to be_empty
  end
  it 'send delete with body' do
    exchange = EchoService.delete(body: { has: 'value' })
    # Creates JSON from Hash
    expect(exchange.request_parameters.body).to eq '{"has":"value"}'
    # Does not send body in headers
    expect(exchange.request_parameters.other_params).not_to have_key :body
    # Response echoed back indicates request body correct
    expect(exchange.format).to eq :json
    expect(exchange.to_hash).to eq('has' => 'value')
  end
end
