# frozen_string_literal: true

require 'support/echo_service'
# Tests for building a request body using a template

CUSTOM_TEMPLATE_FOLDER = 'config/templates'

include Soaspec::RestMethods # Defines Rest methods globally

# Here the template_name is set at the instance of the RestHandler
# This uses the default folder 'templates'
context EchoService.new 'Template', template_name: 'rest_template.json' do
  describe post 'Override title', gloss_title: 'test title' do
    its(['GlossDiv.title']) { is_expected.to eq 'test title' }
  end
  describe post 'Default value' do
    its(['GlossDiv.title']) { is_expected.to eq 'Gloss' }
  end
end

context EchoService.new 'Custom Folder', template_name: 'template.json' do
  before { Soaspec.template_folder = CUSTOM_TEMPLATE_FOLDER }
  describe post 'Default value' do
    its(['GlossDiv.title']) { is_expected.to eq 'Gloss' }
  end
  after { Soaspec.template_folder = 'templates' }
end

context 'Diff temp folder' do
  # Here the template folder is specified before each example. It's assumed this will be constant
  # in a 'spec_helper.rb' and a convention will be followed in a real test suite
  before { Soaspec.template_folder = CUSTOM_TEMPLATE_FOLDER }
  # Here the template_name is set at the instance of the Exchange
  describe EchoService.post(name: 'custom folder', template_name: 'template.json') do
    it 'should read template' do
      # Check correct request body is intended
      expect(subject.request_parameters.body).to include '"title": "Gloss"'
      described_class.call
      # Check correct payload was sent
      payload_string = subject.request.payload.instance_variable_get(:@stream).string
      expect(payload_string).to include '"title": "example glossary"'
    end
  end
  after { Soaspec.template_folder = 'templates' }
end

# Example of setting template within class rather than in Exchange or Handler
class TemplateService < Soaspec::RestHandler
  base_url 'http://localhost:4999/echoer'
  template_name 'template.json'
end

context 'Diff temp folder' do
  before { Soaspec.template_folder = CUSTOM_TEMPLATE_FOLDER }
  describe TemplateService.post do
    it 'can override template step by step' do
      described_class.gloss_title = 'Custom title'
      described_class[:gloss_see] = 'Custom see'
      expect(subject.request_parameters.body).to include '"GlossSee": "Custom see"'
      expect(described_class['GlossDiv.title']).to eq 'Custom title'
      expect(described_class[:GlossSee]).to eq 'Custom see'
    end
  end
  after { Soaspec.template_folder = 'templates' }
end
