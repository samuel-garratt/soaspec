# frozen_string_literal: true

require 'support/blz_soap_service'
# Example of creating SOAP service using a ERB template

context 'BasicSoapHandler', '#template_name' do
  context BLZService.new('Get Bank', template_name: 'soap_template.xml', operation: :get_bank) do
    describe Exchange.new(:default) do
      it { is_expected.to contain_value '70070010' }
      it_behaves_like 'success scenario'
    end
    describe Exchange.new(:override_values, blz: '1000') do
      it { is_expected.to contain_value '1000' }
      it_behaves_like 'success scenario'
    end

    describe Exchange.new(:request_keys_as_strings, 'blz' => '550') do
      it { is_expected.to contain_value '550' }
      it 'has expected keys sent' do
        expect(subject.request_parameters.request_option).to eq :template
        expect(subject.request_parameters.body[:xml]).to include '<tns:blz>550</tns:blz>'
      end
      # TODO: Check request has keys produced
      it_behaves_like 'success scenario'
    end
  end
end
