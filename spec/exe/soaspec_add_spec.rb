# frozen_string_literal: true

describe 'soaspec add rest' do
  before(:all) do
    FileUtils.mkdir_p 'tmp'
    FileUtils.mkdir_p 'tmp/add'
    FileUtils.mkdir_p 'tmp/add/rest'
  end
  let(:create_service) do
    CmdLine.new('ruby ../../../exe/soaspec add rest RestService', chdir: 'tmp/add/rest')
  end
  it 'Creates file correctly' do
    expect(create_service.result).to include 'lib/rest_service.rb'
    expect(create_service.status).to eq 0
  end
end

describe 'soaspec add soap' do
  before(:all) do
    FileUtils.mkdir_p 'tmp'
    FileUtils.mkdir_p 'tmp/add'
    FileUtils.mkdir_p 'tmp/add/soap'
  end
  let(:create_service) { CmdLine.new('ruby ../../../exe/soaspec add soap SoapService', chdir: 'tmp/add/soap') }
  it 'Creates file correctly' do
    expect(create_service.result).to include 'lib/soap_service.rb'
    expect(create_service.status).to eq 0
  end
end
