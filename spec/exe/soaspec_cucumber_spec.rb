# frozen_string_literal: true

describe 'soaspec cucumber' do
  before(:all) do
    FileUtils.mkdir_p 'tmp'
    FileUtils.mkdir_p 'tmp/cuke'
  end
  let(:create_file) { CmdLine.new('ruby ../../exe/soaspec cucumber', chdir: 'tmp/cuke') }
  it 'Creates files correctly' do
    expect(create_file.status).to eq 0
    expect(File.exist?('tmp/cuke/features/step_definitions/generic_steps.rb')).to be true
    expect(File.exist?('tmp/cuke/features/support/env.rb')).to be true
    expect(File.exist?('tmp/cuke/Gemfile')).to be true
  end
end
