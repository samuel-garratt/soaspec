# frozen_string_literal: true

require 'fileutils'

describe 'soaspec generate -w=wsdl' do
  before(:all) do
    FileUtils.mkdir_p 'tmp'
    FileUtils.mkdir_p 'tmp/generate'
  end
  let(:wsdl) { 'http://localhost:4999/BLZService?wsdl' }
  # Command to generate folder structure
  let(:gena_folder) do
    CmdLine.new("ruby ../../exe/soaspec generate -w=#{wsdl} --ci=travis --string_default=123", chdir: 'tmp/generate')
  end
  let(:run_gena_tests) { CmdLine.new('rake', chdir: 'tmp/generate') }
  it 'generate tests pass' do
    puts gena_folder.result
    expect(gena_folder.status).to eq 0
    puts run_gena_tests.result
    expect(run_gena_tests.stdout).to include 'GetBank'
    expect(run_gena_tests.status).to eq 0
  end
end

describe 'soaspec generate' do
  let(:test_port) { '8999' }
  it 'hosts generate page' do
    p_id = Process.spawn('ruby', 'exe/soaspec', 'generate',
                         '--open_browser=false', "--port=#{test_port}",
                         err: %w[logs/test_generate.log w])
    puts "Running generate page on PID '#{p_id}'"
    sleep 2 # Wait for server to start up
    expect(RestClient.get("http://localhost:#{test_port}").code).to eq 200
    Process.kill(:QUIT, p_id)
  end
end
