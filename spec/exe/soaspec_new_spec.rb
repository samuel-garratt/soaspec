# frozen_string_literal: true

# These are integration tests for `soaspec new`. Modules are tested in 'unit' folder
require 'fileutils'

describe 'soaspec new soap' do
  before(:all) do
    FileUtils.mkdir_p 'tmp'
    FileUtils.mkdir_p 'tmp/new'
    FileUtils.mkdir_p 'tmp/new/soap'
  end
  let(:init_folder) do
    CmdLine.new('ruby ../../../exe/soaspec new soap --ci=travis --virtual=false', chdir: 'tmp/new/soap')
  end
  let(:run_init_tests) { CmdLine.new('bundle exec rake spec', chdir: 'tmp/new/soap') }
  it 'init tests pass' do
    puts init_folder.result
    expect(init_folder.status).to eq 0
    `bundle install`
    puts run_init_tests.result
    expect(run_init_tests.stdout).to include 'Test Examples'
    expect(run_init_tests.status).to eq 0
  end
end

describe 'soaspec new rest' do
  before(:all) do
    FileUtils.mkdir_p 'tmp'
    FileUtils.mkdir_p 'tmp/new'
    FileUtils.mkdir_p 'tmp/new/rest'
  end
  let(:init_folder) { CmdLine.new('ruby ../../../exe/soaspec new rest --ci=travis --virtual=true', chdir: 'tmp/new/rest') }
  let(:run_init_tests) { CmdLine.new('bundle exec rspec spec', chdir: 'tmp/new/rest') }
  it 'init tests pass' do
    puts init_folder.result
    expect(init_folder.status).to eq 0
    `bundle install`
    puts run_init_tests.result
    expect(run_init_tests.stdout).to include 'Test extract id'
    expect(run_init_tests.status).to eq 0
  end
end
