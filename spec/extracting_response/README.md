This is where examples of extracting a response are.
It is assumed that this is where the main validation will
occur hence there are shared examples and custom matchers to make extracting from this response.