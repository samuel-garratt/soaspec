# frozen_string_literal: true

require 'support/base_service'
require 'support/blz_soap_service'
# This tests the extraction of xml and hence is an xpath spec along with namespace_spec.rb
context 'attributes' do
  describe BaseService.get 'test_attribute' do
    its(:format) { is_expected.to eq :xml }
    its(:date) { is_expected.to eq '2008-01-10' }
  end
  describe 'SOAP' do
    it 'can extract attributes' do
      BLZService.new(operation: :get_bank, default_hash: { blz: '50' })
      exchange = Exchange.new(:attribute)
      expect(exchange.unique_id).to eq '50'
      expect(exchange.language).to eq 'German'
      expect(exchange.format).to eq :xml
    end
  end
end
