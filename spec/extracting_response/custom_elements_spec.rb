# frozen_string_literal: true

# Explain use of defining an 'element' in a Handler and then using that
# to extract a value

require 'support/blz_soap_service'

context BLZService.new(operation: :get_bank, default_hash: { blz: '5' }) do
  describe Exchange.new(:accessors) do
    its(:bank_name) { is_expected.to eq 'Deutsche Bank' }
  end
end
