# frozen_string_literal: true

# This test is to demonstrate ways of verifying a response is correct.
# Custom matchers and shared examples are demonstrated as short hand way to express what a response
# body should have

# This is a basic ExchangeHandler class with a mandatory base url
class BasicService < Soaspec::RestHandler
  base_url 'http://localhost:4999/echoer'
end

# This class has mandatory elements defined. If you use either the 'be_successful' matcher
# or the 'success scenario' shared example then these will be used.
# When using these matchers, a failure will occur when 'mandatory_elements' are not
# present (no value at $..id) OR there is not a value at ($..success) with a value of 'true'
class MandatoryResponse < Soaspec::RestHandler
  base_url 'http://localhost:4999/echoer'

  mandatory_elements [:id]
  mandatory_json_values success: true
end

describe 'is_successful' do
  # Just checking for http success status not valid
  context 'status code' do
    it 'handles success' do
      expect(BasicService.post(name: 'success url')).to be_successful
    end
    it 'handles failure' do
      expect(
        BasicService.post(name: 'not found url', suburl: 'notfound_url')
      ).not_to be_successful
    end
  end
  describe MandatoryResponse.post(name: 'response body correct', body: { parent: { id: '43543534', success: true } }) do
    it { is_expected.to be_successful } # This verifies that body has mandatory values specified in class
    it_behaves_like 'success scenario' # This does same as above but with a separate test for each check
  end
end

describe BasicService.post(name: 'Empty body', payload: '') do
  it 'throws error checking for element' do
    expect do
      subject['test_path']
    end.to raise_error NoElementAtPath
  end
  it 'throws false using matcher' do
    expect(subject).not_to have_element_at_path 'test_path'
  end
end

describe BasicService.post(name: 'element at path', body: { test: { name: { first: 'bob', last: 'smith' } } }) do
  let(:element_path) { 'name.first' }
  let(:invalid_path) { 'name.middle' }
  context 'element present at path' do
    it { is_expected.to have_element_at_path element_path }
    it 'handles element at path from response' do
      expect(subject.response).to have_element_at_path element_path
    end
    it 'fails if element is not present' do
      expect(subject).to_not have_element_at_path invalid_path
    end
  end
  context 'element has value at path' do
    let(:element_value) { 'bob' }
    it { is_expected.to have_jsonpath_value(element_path => element_value) }
    it 'handles element at path from response' do
      expect(subject.response).to have_jsonpath_value(element_path => element_value)
    end
  end
end
