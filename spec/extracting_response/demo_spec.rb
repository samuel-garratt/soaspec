# frozen_string_literal: true

# This spec is specifically for verifying the demo code within the 'demo' folder at the root of this gem
# works as expected
require 'support/echo_service'

describe EchoService.post(name: 'Demo JSON Extraction',
                          body: { root: { parent1: { child1: '5', child2: 'parent1 word' },
                                          parent2: { uniq: 'val', child2: 'word2' } } }) do
  its(:response) { is_expected.to be_instance_of RestClient::Response }
  its(['uniq']) { is_expected.to eq 'val' }
  its(['$..parent1.child2']) { is_expected.to eq 'parent1 word' }
  its(['$..parent1.uniq,$..parent2.uniq']) { is_expected.to eq 'val' }
  it '.values_at_path' do
    expect(subject.values_from_path('$..child2')).to eq ['parent1 word', 'word2']
  end
  context '.to_hash' do
    it 'returns hash' do
      expect(subject.to_hash).to be_a_kind_of Hash
    end
    it 'can be accessed through Symbol' do
      expect(subject.to_hash[:root][:parent2][:uniq]).to eq 'val'
    end
    it 'can be accessed through String' do
      expect(subject.to_hash['root']['parent2']['uniq']).to eq 'val'
    end
  end
end

describe EchoService.post(name: 'Demo XML Extraction',
                          payload: '<root> <parent1> <child1>5</child1> <child2>parent1 word</child2> </parent1>' \
                                    '<parent2> <uniq attr="test_atr">val</uniq> <child2>word2</child2> </parent2> </root>') do
  its(:response) { is_expected.to be_instance_of RestClient::Response }
  its(['uniq']) { is_expected.to eq 'val' }
  its(['//parent1/child2']) { is_expected.to eq 'parent1 word' }
  its(['//parent1/uniq|//parent2/uniq']) { is_expected.to eq 'val' }
  it '.values_at_path' do
    expect(subject.values_from_path('//child2')).to eq ['parent1 word', 'word2']
  end
  context '.to_hash' do
    let(:response_hash) { subject.to_hash }
    it 'returns hash' do
      expect(response_hash).to be_a_kind_of Hash
    end
    it 'can be accessed through Symbol' do
      expect(response_hash[:root][:parent2][:uniq]).to eq 'val'
    end
    it 'can be accessed through String' do
      expect(response_hash['root']['parent2']['uniq']).to eq 'val'
    end
    it 'can access attribute' do
      expect(response_hash.dig(:root, :parent2, :uniq).attributes['attr']).to eq 'test_atr'
    end
  end
end
