# frozen_string_literal: true

require 'support/blz_soap_service'
require 'support/shared_examples'

# This is older, longer style of setting Handler. Handler params can be set within new method
error_example = BLZService.new('Error example')
error_example.operation = :get_bank
error_example.default_hash = {}

context 'error scenarios' do
  context error_example do
    describe Exchange.new(:no_blz_error) do
      it_behaves_like 'error scenario'
      its(:error_message) { is_expected.to include 'Unexpected subelement' }
    end
    describe Exchange.new('Bank not found', blz: 'Bank of nowhere') do
      it_behaves_like 'error scenario'
      its(:error_message) { is_expected.to include 'Bank of nowhere' }
    end
  end
end
