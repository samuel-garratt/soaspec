# frozen_string_literal: true

require 'support/html_service'

describe 'HTML response' do
  it 'can be accessed via XPath' do
    exchange = HTMLService.get 'html_doc'
    expect(exchange['//h1']).to eq 'Heading'
  end
  it 'can use element' do
    exchange = HTMLService.get 'html_doc'
    expect(exchange.heading).to eq 'Heading'
  end
end
