# frozen_string_literal: true

# Spec for finding values via multiple JSON paths
require 'support/echo_service'

describe EchoService.post(name: 'Json Path',
                          body: { test: { nested: { id: '55' }, empty: '' }, id: '33', success: true }) do
  its(:format) { is_expected.to eq :json }
  its([:id]) { is_expected.to eq '33' }
  it 'can match multiple json paths comma separated' do
    expect(subject.id).to eq '33'
  end
  it 'find specified it' do
    expect(subject['$..nested.id']).to eq '55'
  end
  it 'find path through []' do
    expect(subject['success']).to eq true
  end
  it 'finds empty value' do
    expect(subject['empty']).to eq ''
  end
  it 'does not find incorrect path' do
    expect { subject[:Id] }.to raise_error NoElementAtPath
  end
end

# Using 'pascal_keys', elements in JSONPath are checked in both PascalCase and snake_case
# when only a 'snakecase' matcher is used
class EchoPascalService < Soaspec::RestHandler
  base_url 'http://localhost:4999/echoer'
  pascal_keys true
end

include Soaspec::RestMethods

describe EchoPascalService.new('PascalService') do
  describe post :extraction, body: { 'Success': true, data: [key1: 'value1', key2: 'value2'] } do
    # This demonstrates how snakecase can be given in assertion but a 'PascalCase' value will also
    # be extracted
    its([:success]) { is_expected.to eq true }
    # Shows snakecase values can still be obtained
    its([:key2]) { is_expected.to eq 'value2' }
    # Example of obtaining value using 1 path or another path with ','
    # In this case both elements mentioned will also be searched with their Pascal Path
    # The first key3 does not return a value. No error is raised, the second path will be used
    its(['key3,key2']) do
      is_expected.to eq 'value2'
    end
    it 'raises correct error' do
      expect { described_class['key3,key4'] }.to raise_error(NoElementAtPath) do |error|
        expect(error.message).to include '"$..key3", "$..Key3", "$..key4", "$..Key4"'
      end
    end
  end
end
