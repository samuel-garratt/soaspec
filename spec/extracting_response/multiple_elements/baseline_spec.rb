# frozen_string_literal: true

# Class used to show a requests can be made against an endpoint, their
# responses recorded and then used in expectations following pattern based
# on request parameters
class BaseLine < Soaspec::RestHandler
  base_url "#{VIRTUAL_SERVER_LOCATION}/baseline"
end

describe 'Hash baseline' do
  it 'creates expected file if not present' do
    exchange = BaseLine.get suburl: %w[1 3], q: { param1: 12_132, param2: 22_132,
                                                  very_long_name: 'A long value too' }
    expect(exchange).to be_successful
    file = Soaspec::Baseline.new(exchange, :hash).file
    FileUtils.rm file, force: true
    expect do
      expect(exchange).to match_baseline :hash
    end.to raise_error Soaspec::BaselineError, /#{file}/
  end
  it 'asserts baseline when file is present' do
    exchange = BaseLine.get suburl: %w[1 4], q: { param1: 1, param2: 2 }
    expect(exchange).to be_successful
    file = Soaspec::Baseline.new(exchange, :hash).file
    expect(File.exist?(file)).to be true
    expect(exchange).to match_baseline :hash
  end
  it 'handles empty suburl and query' do
    exchange = BaseLine.get
    expect(exchange).to be_successful
    expect(exchange).to match_baseline :hash
  end
end

describe 'Raw baseline' do
  it 'handles empty suburl and query' do
    exchange = BaseLine.get
    expect(exchange).to be_successful
    expect(exchange).to match_baseline
  end
end
