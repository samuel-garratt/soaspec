# frozen_string_literal: true

require 'support/base_service'

context 'multiple values' do
  context 'xml' do
    describe BaseService.get(suburl: 'test_attribute') do
      subject { described_class.values_from_path('//comment_line') }
      its(:count) { is_expected.to be > 1 }
      it { is_expected.to eq ['First comment', 'Second comment'] }
      it 'can handle working with Hash' do
        expect(described_class.to_hash.dig(:note, :@date)).to eq '2008-01-10'
      end
    end
  end
  context 'json' do
    describe BaseService.get(suburl: 'test/multiple_json') do
      subject { described_class.values_from_path('$..price') }
      its(:count) { is_expected.to be > 1 }
      it { is_expected.to eq [19.95, 8.95, 12.99, 8.99, 22.99] }
      it 'can handle working with Hash' do
        expect(described_class.to_hash.dig('store', 'bicycle', 'price')).to eq 19.95
      end
      it 'has count of 0 for no matching values' do
        expect(described_class.values_from_path('$..path_where_nothing_is')).to eq []
      end
    end
  end
end
