# frozen_string_literal: true

require 'support/blz_soap_service'
# This is an example of extracting multiple values with the same XPath from a response
multiple_ort_bank = 20

context BLZService.new(operation: :get_bank) do
  describe Exchange.new(:not_within_context, blz: multiple_ort_bank) do
    subject { described_class.values_from_path('//ns1:ort') }
    it { is_expected.to be_instance_of Array }
    its(:count) { is_expected.to be > 1 }
    it { is_expected.to eq %w[München Wellington Tokyo] }
  end
end
