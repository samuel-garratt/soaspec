# frozen_string_literal: true

require 'support/base_service'

# Example of setting strip namespaces which will remove namespaces WHEN
# no namespace is used in the path to obtain from the response
class StripNamespaceService < BaseService
  strip_namespaces true
end

context 'testing namespaces' do
  describe BaseService.get(suburl: 'namespace') do
    it_behaves_like 'success scenario'
    context 'strip_namespaces default is false' do
      its(['h:td']) { is_expected.to eq 'Apples' }
    end
  end
  describe StripNamespaceService.get(suburl: 'namespace') do
    it 'Does not need namespace' do
      expect(subject['td']).to eq 'Apples'
    end
    it 'Can still handle namespace' do
      expect(subject['f:td']).to eq 'Wood'
    end
  end
end
