# frozen_string_literal: true

# Example of a service used to get a plain text response with a regular expression to extract from it
class TextResponse < Soaspec::RestHandler
  base_url "#{VIRTUAL_SERVER_LOCATION}/text_response"
end

# This tests the extraction of xml and hence is an xpath spec along with namespace_spec.rb
context 'testing attributes' do
  describe TextResponse.get do
    it_behaves_like 'success scenario'
    it 'can have text extracted via reg expression' do
      expect(described_class['ID=.*'].split[0]).to eq 'ID=12345'
    end
    its(:format) { is_expected.to eq :string }
  end
end
