# frozen_string_literal: true

# Verifying that an exchange can be reached from within a response
require 'support/echo_service'

describe Soaspec::RestHandler do
  let(:exchange) { EchoService.post(body: { id: 1, val: { item: 1 }, val2: { item: 2 } }) }
  let(:response) { exchange.response }
  it 'can access exchange that made response' do
    expect(response.exchange).to equal exchange
  end
  it 'can extract path from response' do
    expect(response.value_from_path(:id)).to eq 1
  end
  it 'can extract elements at multiple paths from response' do
    expect(response.values_from_path('item')).to eq [1, 2]
  end
end
