# frozen_string_literal: true

# This test ensures the virtual service that simulates different ids works as expected
# It also shows an example of how tests could be structured

# Has several ids though return a 'false' value before a 'developed' option is set
class PackageService < Soaspec::RestHandler
  base_url "#{VIRTUAL_SERVER_LOCATION}/packages"
end

include Soaspec::RestMethods

context PackageService.new('Test extract id') do
  before(:all) do
    PackageService.post(suburl: 'developed', payload: 'false').call
  end
  describe get 'Negative', suburl: '4/40' do
    its(['success']) { is_expected.to eq 'false' }
  end

  describe get 'Positive', suburl: '4/41' do
    its(['success']) { is_expected.to eq 'true' }
  end

  describe 'After developed' do
    it 'negative become true' do
      expect(PackageService.post(suburl: 'developed', payload: 'true').response.body).to include 'true'
      expect(PackageService.get(suburl: '4/40')['success']).to eq 'true'
    end
  end
end
