# frozen_string_literal: true

require 'simplecov'
SimpleCov.start do
  add_filter 'vendor'
end

SimpleCov.at_exit do
  SimpleCov.result.format!
  SimpleCov.minimum_coverage 95
end

VIRTUAL_SERVER_LOCATION = 'http://localhost:4999'

require 'bundler/setup'
require 'soaspec'
require 'data_magic'
require 'open3' # Used for handling stdout
require 'factory_bot'

Soaspec::SpecLogger.log_api_traffic = true # Make this false if you do not want to see logs
# Soaspec::SpecLogger.traffic_folder = 'tmp_logs' Example of changing log folder
Soaspec::SpecLogger.output_to_terminal = true # Use this option if you want logs to be displayed in STDOUT
# Example of fixing a location of traffic file
Soaspec::SpecLogger.traffic_file = 'custom.log'
# Change color of terminal output
Soaspec::SpecLogger.terminal_color = :magenta

RSpec.configure do |config|
  include DataMagic # Used as example of loading data smartly
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  config.include Soaspec::RestMethods # Only include this if you want to use REST 'get', 'post' methods defined globally

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.around(:each) do |example|
    Soaspec::SpecLogger.log_api_traffic = false if example.metadata[:no_log]
    example.run
    Soaspec::SpecLogger.log_api_traffic = true # Reset log status
  end

  config.after(:each) do |_example|
    # You could add this uncomment this line to get custom description out of API test failures
    # puts "Debugging #{example.full_description}. Response body is: " + Soaspec.last_exchange.response.body if example.exception
  end

  # Close test server after all RSpec tests have run
  config.after(:suite) do
    Process.kill(:QUIT, ENV['test_server_pid'].to_i) if ENV['test_server_pid'] # && Process.wait - causes failure
  end
end

# Makes accessing results of commands via command line easy
class CmdLine
  # Easy access to status of command line run
  attr_reader :stdout, :stderr, :status

  # @param [String] command_line_to_run Command to run on terminal
  # @param [String] options Open3 options, E.g, chdir to execute within a folder
  def initialize(command_line_to_run, options)
    @name = command_line_to_run
    @stdout, @stderr, @status = Open3.capture3(command_line_to_run, options)
  end

  # @return [String] Result of executing Terminal instruction with STDOUT, STDERR, and Status
  def result
    stdout + stderr + status.to_s
  end

  # Return name of command line typed
  def to_s
    @name
  end
end
