# Explanation

Common classes used to support tests are within here. For classes that are specific to the test, they
will lie within the `spec` file itself.