# frozen_string_literal: true

class BaseService < Soaspec::RestHandler
  base_url VIRTUAL_SERVER_LOCATION

  attribute(:date) # This will automatically search for an attribute called 'date'
end
