# frozen_string_literal: true

require 'soaspec'

# This class is not part of the gem. It's an example of a class you can make
# to describe your APIs. Usually this would exist in the 'lib' directory
# Common configuration for the Savon client should go here
class BLZService < Soaspec::SoapHandler
  wsdl "#{VIRTUAL_SERVER_LOCATION}/BLZService?wsdl"
  basic_auth 'username', 'password'
  soap_version 2 # Default
  raise_errors false # Default
  ssl_verify_mode :peer #:none
  namespaces({})

  # Add to or override default Savon client options
  def savon_options
    {
      # Example of old way of setting values
      # wsdl: "#{VIRTUAL_SERVER_LOCATION}/BLZService?wsdl",
      # basic_auth: %w[username password]
    }
  end

  convert_to_lower true # All xpath will be done with XML that is converted to lower case
  strip_namespaces true # This allows namespace not to be used. Be careful with this

  element(:bank_name, 'bezeichnung')
  element(:error_message, :text)
  attribute(:unique_id)
  attribute(:language, 'lang')

  # # Specifying that get_weather_result must be present in the SOAP response
  mandatory_elements [:plz]

  # Example of xpath value that must be true for all success scenarios
  mandatory_xpath_values 'ns1:bezeichnung' => 'Deutsche Bank'

  # Example of setting an attribute on the root XML element
  root_attributes 'Version' => '1' # TODO: Create test on request for this
end
