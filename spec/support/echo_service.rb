# frozen_string_literal: true

class EchoService < Soaspec::RestHandler
  base_url "#{VIRTUAL_SERVER_LOCATION}/echoer"

  element :id, 'Id,id' # 2 ways of accessing id
end
