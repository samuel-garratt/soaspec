# frozen_string_literal: true

class HTMLService < Soaspec::RestHandler
  base_url VIRTUAL_SERVER_LOCATION

  element(:heading, '//h1')
end
