# frozen_string_literal: true

# Class to demonstrate how oauth2 parameters can be specified and used in a RestHandler
class OAuthSvc < Soaspec::RestHandler
  # Filename of oauth2 file to use for oauth2 parameters
  oauth2_file 'oauth2' # This will add a default 'Authorization: Bearer access_token' header automatically

  headers client_id: client_id # 'THIS IS A TEST SECRET ID DSFJDSLKFJ'

  element :customer_id, :customer_id

  base_url "#{VIRTUAL_SERVER_LOCATION}/invoice"
  mandatory_elements ['customer_id']
end
