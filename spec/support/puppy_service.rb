# frozen_string_literal: true

require 'soaspec'

class PuppyService < Soaspec::RestHandler
  headers accept: 'application/json', content_type: 'application/json'

  # Old way of setting headers. TODO: Make test to demonstrate other way of setting params
  # def rest_resource_options
  #   {
  #     headers:
  #         {
  #           accept: 'application/json',
  #           content_type: 'application/json'
  #         }
  #   }
  # end

  base_url 'https://petstore.swagger.io/v2/pet'

  element :id, :id
  element :category_id, '$..category.id'
end
