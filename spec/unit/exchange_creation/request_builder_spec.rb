# frozen_string_literal: true

require 'support/echo_service'

describe 'Exchange.add' do
  before { EchoService.use }
  it 'Adds a non existent suburl' do
    exchange = Exchange.new
    exchange + { suburl: 'test' }
    expect(exchange.request_parameters.suburl).to eq 'test'
  end
  it 'Adds to an existing suburl' do
    exchange = Exchange.new suburl: 'subpath'
    exchange.add suburl: '/test'
    expect(exchange.request_parameters.suburl).to eq 'subpath/test'
  end
  context 'Parameter as Hash query)' do
    it 'Sets afresh' do
      exchange = Exchange.new 'No query', suburl: 'test'
      exchange.add q: { name: 'test' }
      expect(exchange.request_parameters.query).to eq(name: 'test')
    end
    it 'Adds to it' do
      exchange = Exchange.new 'No query', q: { ab: 'te', cd: 'st' }
      exchange.add q: { ef: 'new' }
      query = exchange.request_parameters.query
      expect(query).to eq(ab: 'te', cd: 'st', ef: 'new')
    end
  end
end
