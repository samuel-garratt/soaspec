# frozen_string_literal: true

# Call virtual API echoing back response
class EchoService < Soaspec::RestHandler
  base_url "#{VIRTUAL_SERVER_LOCATION}/echoer"
end

describe Soaspec::RestMethods do
  before do
    # extend described_class Use this if you don't want to define this globally in RSpec class
    EchoService.new('Demo REST methods').use
  end

  it 'can create a post' do
    exchange = post(:create_data, body: { subject: 'Minimal' })
    puts exchange.inspect # TODO: Nice to easily be able to see request params including what's come from ExchangeHandler
    exchange.call
    expect(exchange.class).to eq Exchange
    expect(exchange.request.method).to eq 'post'
  end
end
