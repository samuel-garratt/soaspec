# frozen_string_literal: true

shared_examples_for 'exchange matcher' do
  context 'Custom matcher methods make testing efficient' do
    it { is_expected.to respond_to 'include_value?' }
    it { is_expected.to respond_to 'include_in_body?' }
    it { is_expected.to respond_to 'found?' }
    it { is_expected.to respond_to 'include_key?' }
    it { is_expected.to respond_to 'value_from_path' }
  end
end
