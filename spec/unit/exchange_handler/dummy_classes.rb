# frozen_string_literal: true

class DummySoap < Soaspec::SoapHandler
  def savon_options
    {
      wsdl: 'http://www.webservicex.com/globalweather.asmx?wsdl' # wsdl must be set
    }
  end
end

class DummyRest < Soaspec::RestHandler
  base_url 'http://dummy_http' # Mandatory value needing to be set
end
