# frozen_string_literal: true

require_relative 'dummy_classes'
require_relative 'requestor_examples'
require_relative 'correct_matchers_examples'

context 'ExchangeHandlers must handle requests' do
  describe DummySoap.new('Soap Handler') do
    it_behaves_like 'requestor'
    it_behaves_like 'exchange matcher'
  end

  describe DummyRest.new('Rest Handler') do
    it_behaves_like 'requestor'
    it_behaves_like 'exchange matcher'
  end
end
