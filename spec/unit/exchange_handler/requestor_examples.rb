# frozen_string_literal: true

shared_examples_for 'requestor' do
  context 'ExchangeHandlers must handle requests' do
    it { is_expected.to respond_to 'make_request' }
  end
end
