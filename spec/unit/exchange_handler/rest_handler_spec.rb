# frozen_string_literal: true

class NilHeaderHandler < Soaspec::RestHandler
  headers test: 'header', fail: nil # Nil header value will cause ArgumentException
  base_url 'this is mandatory field'
end

class DefaultHash < Soaspec::RestHandler
  base_url 'this is mandatory field'
  default_hash test: 1, test2: 2
end

class DefaultTemplate < Soaspec::RestHandler
  base_url 'this is mandatory field'
  template_name 'rest_template.json'
end

class DoubleMethodHandler < Soaspec::RestHandler
  base_url 'this is mandatory field'
  default_hash test: 1, test2: 2
  template_name 'rest_template.json'
end

# Example of using ERB to make URL dynamic based on an Environment variable
class MultiEnvironmentHandler < Soaspec::RestHandler
  ENV['env'] = 'uat'
  base_url "https://test_serice/<%= ENV['env'] %>"
end

class CustomException < Soaspec::RestHandler
  base_url "#{VIRTUAL_SERVER_LOCATION}/echoer"

  # Throw exception on this condition
  after_response do |response, handler|
    raise Soaspec::ResponseError if handler.value_from_path(response, 'error')
  end
end

class CustomRetryClasses < Soaspec::RestHandler
  base_url VIRTUAL_SERVER_LOCATION

  retry_on_exceptions [RestClient::InternalServerError], pause: 0.1, count: 5
end

describe Soaspec::RestHandler do
  it 'throws error on nil header value' do
    expect do
      NilHeaderHandler.new
    end.to raise_error ArgumentError, /Header 'fail' is null/
  end
  it 'throws error if both default_hash and template_name set' do
    expect do
      DoubleMethodHandler.new
    end.to raise_error ArgumentError, /Cannot define both template_name and default_hash/
  end
  it 'create with correct default hash' do
    default = DefaultHash.new
    expect(default.instance_variable_get(:@default_hash)).to eq(test: 1, test2: 2)
    expect(default.instance_variable_get(:@request_option)).to eq :hash
  end
  it 'create with class template' do
    default = DefaultTemplate.new
    expect(default.instance_variable_get(:@request_option)).to eq :template
    expect(default.instance_variable_get(:@template_name)).to eq 'rest_template.json'
  end
  it 'can interpret erb in base_url' do
    uat = MultiEnvironmentHandler.new
    expect(uat.base_url_value).to end_with 'uat'
  end
  it 'can throw custom exception' do
    bad_request = CustomException.post(body: { error: 'true' })
    expect do
      bad_request.response
    end.to raise_error Soaspec::ResponseError
  end
end

describe CustomRetryClasses do
  before { @handler = CustomRetryClasses.use }
  it 'retries on InternalServerError' do
    exchange = get(:error, suburl: '/server_error')
    exchange.call
    expect(@handler.exception).to be_instance_of RestClient::InternalServerError
    expect(exchange.times_retried).to eq 5
    expect(exchange.status_code).to eq 500
  end
  it 'does not retry if successful' do
    exchange = get(:redirect)
    exchange.call
    expect(@handler.exception).to be nil
    expect(exchange.times_retried).to eq nil
    expect(exchange.status_code).to eq 200
  end
end
