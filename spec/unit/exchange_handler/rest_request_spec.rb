# frozen_string_literal: true

require 'support/echo_service'

# RestHandler with a default hash set
class HashService < EchoService
  default_hash a: 1
end

class RestTemplateService < EchoService
  template_name 'rest_template.json'
end

describe Soaspec::RestRequest, 'Empty(Defaults)' do
  subject { Soaspec::RestRequest.new({}, {}, EchoService.new) }
  its(:suburl) { is_expected.to eq nil }
  its(:body) { is_expected.to eq nil }
  its(:method) { is_expected.to eq :post }
  its(:suburl) { is_expected.to eq nil }
end

describe Soaspec::RestRequest, 'Hash body' do
  subject { Soaspec::RestRequest.new({ method: :post }, {}, HashService.new) }
  let(:overriden) do
    Soaspec::RestRequest.new({ method: :post, body: { b: 2 } }, {}, HashService.new)
  end
  its(:body_params) { is_expected.to eq(a: 1) }
  its(:body) { is_expected.to eq('{"a":1}') }
  it 'can have body overridden' do
    expect(overriden.body_params).to eq(a: 1, b: 2)
  end
end

describe Soaspec::RestRequest, 'template body' do
  subject { Soaspec::RestRequest.new({ method: :post }, {}, RestTemplateService.new) }
  let(:override_values) { { gloss_title: 'Custom Title' } }
  let(:overriden_global) do
    Soaspec::RestRequest.new({ method: :post, **override_values }, {}, RestTemplateService.new)
  end
  let(:overriden_local) do
    Soaspec::RestRequest.new({ method: :post, body: { **override_values } }, {}, RestTemplateService.new)
  end
  its(:body) { is_expected.to include 'example glossary' }
  it 'can have parameters set globally' do
    expect(overriden_global.body).to include '"title": "Custom Title"'
    expect(overriden_global.body_params).to include(gloss_title: 'Custom Title')
  end
  it 'can have params set within body' do
    expect(overriden_local.body).to include '"title": "Custom Title"'
    expect(overriden_local.body_params).to eq(gloss_title: 'Custom Title')
  end
end

describe Soaspec::RestRequest, 'full_url' do
  let(:slash_pre_existing) { Soaspec::RestRequest.new({ suburl: '/path' }, {}, EchoService.new) }
  let(:no_slash) { Soaspec::RestRequest.new({ suburl: 'path' }, {}, EchoService.new) }
  let(:expected_path) { "#{VIRTUAL_SERVER_LOCATION}/echoer/path" }
  it 'handles slash on one end end' do
    expect(slash_pre_existing.full_url).to eq expected_path
  end
  it 'adds slash when not present' do
    expect(no_slash.full_url).to eq expected_path
  end
end

describe Soaspec::RestRequest, 'Get query' do
  let(:query_param) do
    { par1: 1, par2: 2 }
  end
  let(:subject) { Soaspec::RestRequest.new({ method: :get, q: query_param }, {}, EchoService.new) }
  its(:body) { is_expected.to eq nil }
  its(:query) { is_expected.to eq(query_param) }
  its(:other_params) { is_expected.to eq(params: query_param) }
end

describe 'Extracting Rest Resource request' do
  it 'handles query params' do
    exchange = EchoService.get(q: { p1: 1, p2: 2 })
    exchange.call
    expect(exchange.request.url).to end_with('?p1=1&p2=2')
  end
end
