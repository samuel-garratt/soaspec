# frozen_string_literal: true

# Tests properties that can be set in class inheriting exchange

# Handler that will always return a server error
class ErrorService < Soaspec::RestHandler
  base_url 'http://localhost:4999/server_error'
end

# @return [Integer] Number of times to retry
RETRY_COUNT = 2

class PositiveExchange < Exchange
  expect_positive_status retry_count: RETRY_COUNT
end

describe 'Exchange.expect_positive_status' do
  context ErrorService do
    let(:exchange) do
      ErrorService.use
      PositiveExchange.new('Retrier', method: :get)
    end
    it 'sets retry_count' do
      expect(exchange.retry_count).to eq RETRY_COUNT
    end
    it '.retry_for_success? - true' do
      expect(exchange.retry_for_success?).to be true
    end
    it 'returns failure status code' do
      expect(exchange).not_to be_successful
    end
    it "makes request 'RETRY_COUNT + 1' (#{RETRY_COUNT + 1}) times" do
      number_of_tries = RETRY_COUNT + 1
      handler = exchange.exchange_handler
      expect(handler).to receive(:make_request).exactly(number_of_tries).times.and_call_original
      exchange.call
      expect(handler.exception).to be_instance_of RestClient::InternalServerError
      expect(exchange.times_retried).to eq RETRY_COUNT
      expect(exchange.status_code).to eq 500
    end
  end
end
