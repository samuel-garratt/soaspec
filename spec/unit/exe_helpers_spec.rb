# frozen_string_literal: true

require 'soaspec/wsdl_generator'

describe Soaspec::WsdlGenerator do
  before(:each) { extend Soaspec::ExeHelpers }
  let(:options) { {} }
  context '#create_files_for correct files' do
    it 'soap' do
      expect(self).to receive(:create_file).at_least(4).times
      create_files_for('soap')
    end
    it 'rest' do
      expect(self).to receive(:create_file).at_least(2).times
      create_files_for('rest')
    end
  end
  it 'creates multiple files easily' do
    expect(self).to receive(:create_file).exactly(3).times
    create_files %w[a b c]
  end
  context '#spec_task' do
    context 'virtual' do
      let(:options) { { virtual: true } }
      it 'dependency on virtual service' do
        expect(spec_task).to include ':start_test_server'
      end
    end
    context 'not virtual' do
      let(:options) { { virtual: false } }
      it 'no dependency on virtual service' do
        expect(spec_task).not_to include ':start_test_server'
      end
    end
  end
  context '#retrieve_contents' do
    it 'reads content from generated folder' do
      expect(retrieve_contents('Rakefile', true)).to include 'task default: :spec'
    end
  end
  context '#create_file' do
    let(:test_file) { 'test_file' }
    after(:each) { FileUtils.rm test_file }
    it 'can create a file with contents provided' do
      create_file(filename: test_file, content: 'test')
      expect(File.read(test_file).strip).to eq 'test'
    end
    it 'handles an existing file' do
      File.write test_file, 'Different content'
      expect do
        create_file(filename: test_file, content: 'test')
      end.not_to raise_exception
    end
  end
  it 'creates folder if not exists' do
    expect(FileUtils).to receive :mkdir_p
    create_folder 'folder_not_present'
  end
  context 'Creating dynamic SOAP files' do
    let(:options) { { name: 'TestClass' } }
    let(:test_operation) { 'test_soap_operation' }
    it 'can create a class for particular name' do
      expect(class_content).to include 'def savon_options'
    end
    it 'can create a spec for soap operation' do
      expect(generated_soap_spec_for(test_operation)).to include 'test_soap_operation = TestClass.new'
    end
  end
end
