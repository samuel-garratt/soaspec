# frozen_string_literal: true

require 'sinatra'
require 'rack'
require 'rack/test'
require 'rspec'

ENV['RACK_ENV'] = 'test'

require 'soaspec/generate_server'

def cleanup_files
  %w[lib/test_api.rb credentials/test_api.yml].each do |file|
    FileUtils.rm file, force: true
  end
end

describe Soaspec::GenerateServer do
  let(:browser) do
    Rack::Test::Session.new(Rack::MockSession.new(Soaspec::GenerateServer))
  end
  it 'hosts web page' do
    browser.get '/'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include '<h1>REST Exchange generator</h1>'
  end
  it 'has css' do
    browser.get '/css/bootstrap.css'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include 'html {'
  end
  context 'generates' do
    before { cleanup_files }
    after { cleanup_files }
    it 'generates' do
      # TODO: Better approach would be something like below (Need help)
      # expect(self).to receive(:create_file).exactly(1).times
      browser.post '/generate', className: 'TestApi',
                                baseUrl: 'http://testurl', basicAuthUser: ''
      expect(browser.last_response.status).to eq 302
      expect(File.exist?('lib/test_api.rb'))
    end
  end
end
