# frozen_string_literal: true

# Exists for interpreter to throw other
class Custom
end

describe Interpreter do
  let(:leading_char_response) { "\n\r <test><to>1></to></test>" }
  let(:xml_with_encoding) do
    '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><pets><Pet>Smokey</Pet></pets>'
  end
  let(:basic_json) { '   { "test": "value" }' }
  let(:array_json) { '  [ { "test": "value" }, { "test2": "value2" } ]' }
  let(:json_with_xml) { ' "test": "value", "xml": "<tag>" }' }
  let(:html_doc) do
    '<!doctype html>
<html lang="en">
<head></head>
<body><h1>Heading</h1></body>
</html>'
  end
  let(:plain_string) { 'This is some text. ID=12345 value' }
  it 'handles xml with leading chars' do
    expect(described_class.response_type_for(leading_char_response)).to eq :xml
  end
  context 'handles json that is' do
    it 'basic' do
      expect(described_class.response_type_for(basic_json)).to eq :json
    end
    it 'array' do
      expect(described_class.response_type_for(array_json)).to eq :json
    end
    it 'with xml' do
      expect(described_class.response_type_for(array_json)).to eq :json
    end
  end
  it 'handles XML document' do
    xml = Nokogiri::XML '<root><test>val</test></root>'
    expect(described_class.response_type_for(xml)).to eq :xml
  end
  it 'handles Hash' do
    expect(described_class.response_type_for(a: 1, b: 2)).to eq :hash
  end
  it 'handles HTML' do
    expect(described_class.response_type_for(html_doc)).to eq :html
  end
  it 'handles unknown' do
    expect(described_class.response_type_for(Custom.new)).to eq :unknown
  end
  it 'handles string' do
    expect(described_class.response_type_for(plain_string)).to eq :string
  end
  it 'handles XML with encoding' do
    puts described_class.response_type_for(xml_with_encoding)
  end
end
