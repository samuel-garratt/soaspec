# frozen_string_literal: true

require 'rspec'

ENV['env'] = 'uat'

describe Soaspec::OAuth2 do
  let(:username) { "username.<%= ENV['env'] %>" }
  let(:client_id) { 'fsjdlkfsdjlfkjsdlkf' }
  let(:client_secret) { 'fsdfSECRETdsfsf' }
  let(:token_url) { 'localhost:4999/as/token.oauth2' }
  let(:client_credentials) { { username: username, client_id: client_id, client_secret: client_secret, token_url: token_url } }
  let(:string_credentials) { { 'username' => username, 'client_id' => client_id, 'client_secret' => client_secret, 'token_url' => token_url } }
  let(:creds_without_token_url) do
    client_credentials.delete(:token_url)
    client_credentials
  end
  it 'should raise error without client id and secret' do
    expect do
      Soaspec::OAuth2.new({ username: username }, '')
    end.to raise_error ArgumentError, /client_id and client_secret/
  end
  it 'should allow parameters as string' do
    client = Soaspec::OAuth2.new(string_credentials)
    expect(client.payload.keys).to eq %i[client_id client_secret grant_type]
  end
  context 'client credentials' do # No username or password specified
    subject(:client) { Soaspec::OAuth2.new(client_credentials) }
    subject(:override_oauth) { Soaspec::OAuth2.new(client_credentials, 'override') }
    it 'should extract parameters via erb' do
      expect(client.params[:username]).to eq 'username.uat'
    end
    it 'setting api_username should override user' do
      expect(override_oauth.params[:username]).to eq 'override'
    end
    it 'have payload' do
      expect(client.payload.keys).to eq %i[client_id client_secret grant_type]
    end
    it 'grant_type of client_credentials' do
      expect(client.payload[:grant_type]).to eq 'client_credentials'
    end
    it 'defaults to no multipart' do
      expect(subject.payload[:multipart]).to eq nil
    end
    context 'Setting multipart manually' do
      it 'can have multipart set false' do
        auth = Soaspec::OAuth2.new(client_credentials.merge(multipart: false), nil)
        expect(auth.payload[:multipart]).to eq nil
      end
      it 'can have multipart set true' do
        auth = Soaspec::OAuth2.new(client_credentials.merge(multipart: true), nil)
        expect(auth.payload[:multipart]).to eq true
      end
    end
  end
  context 'grant_type - password' do
    subject { Soaspec::OAuth2.new(client_credentials.merge(password: 'test'), nil) }
    its(:password) { is_expected.to eq 'test' }
    it 'defaults to multipart' do
      expect(subject.payload[:multipart]).to eq true
    end
    context 'Setting multipart manually' do
      it 'can have multipart set false' do
        auth = Soaspec::OAuth2.new(client_credentials.merge(password: 't', multipart: false), nil)
        expect(auth.payload[:multipart]).to eq nil
      end
      it 'can have multipart set true' do
        auth = Soaspec::OAuth2.new(client_credentials.merge(password: 't', multipart: true), nil)
        expect(auth.payload[:multipart]).to eq true
      end
    end
  end
  it 'allows resource to be set' do
    resource_server = 'https://demo_api_server'
    auth = Soaspec::OAuth2.new(client_credentials.merge(resource: resource_server,
                                                        password: 't'), nil)
    expect(auth.payload[:resource]).to eq resource_server
  end
  context 'token_url' do
    it 'use default' do
      Soaspec::OAuth2.token_url = token_url
      client = Soaspec::OAuth2.new(creds_without_token_url)
      expect(client.params[:token_url]).to eq token_url
      Soaspec::OAuth2.token_url = nil
    end
    it 'raise exception not set anywhere' do
      Soaspec::OAuth2.token_url = nil
      expect do
        Soaspec::OAuth2.new(creds_without_token_url)
      end.to raise_exception ArgumentError
    end
  end
  context 'access token' do
    it 'can be stored once and then reused' do
      Soaspec::OAuth2.refresh_token = :once
      first_client = Soaspec::OAuth2.new(client_credentials)
      first_client.access_token # first call to get access token
      second_client = Soaspec::OAuth2.new(client_credentials)
      # Response should not be called when retrieving access token again (following line)
      expect(second_client).not_to receive(:response)
      second_client.access_token # Second call should use stored token
      expect(Soaspec::OAuth2.access_tokens[second_client.params]).to start_with 'TEST_TOKEN' # Storage made
      Soaspec::OAuth2.refresh_token = :always
    end
    it 'can be retrieved freshly every time needed' do
      Soaspec::OAuth2.refresh_token = :always
      Soaspec::OAuth2.new(client_credentials).access_token
      second_client = Soaspec::OAuth2.new(client_credentials)
      expect(second_client).to receive(:response).and_call_original
      second_client.access_token
    end
  end
  context 'instance urls' do
    it 'they are stored once and then reused' do
      first_client = Soaspec::OAuth2.new(client_credentials)
      first_client.instance_url # first call to get instance url
      second_client = Soaspec::OAuth2.new(client_credentials)
      expect(second_client).not_to receive(:response) # Response should not be called when retrieving access token again (next line)
      second_client.instance_url # Second call should use stored instance url
      expect(first_client.instance_url).to eq second_client.instance_url
    end
  end
  context 'Rest exceptions' do
    it 'should set retry_count' do
      expect(Soaspec::OAuth2.new(client_credentials).retry_count).to eq 0
    end
    it 'retry on exception' do
      creds = client_credentials
      creds[:token_url] = 'localhost:4999/does_not_exist'
      client = Soaspec::OAuth2.new(creds)
      expect { client.access_token }.to raise_exception RestClient::NotFound
      expect(client.retry_count).to eq 2
    end
    it 'can override retry limit' do
      Soaspec::OAuth2.retry_limit = 0
      creds = client_credentials
      creds[:token_url] = 'localhost:4999/does_not_exist'
      client = Soaspec::OAuth2.new(creds)
      expect { client.access_token }.to raise_exception RestClient::NotFound
      expect(client.retry_count).to eq 1
    end
  end
  context 'Logging' do
    it 'outputs request message by default' do
      client = Soaspec::OAuth2.new(client_credentials)
      expect(client).to receive(:request_message)
      client.access_token
    end
    it 'can have request message description disabled' do
      Soaspec::OAuth2.request_message = false
      client = Soaspec::OAuth2.new(client_credentials).access_token
      Soaspec::OAuth2.request_message = true
      expect(client).not_to receive(:request_message)
    end
  end
end

describe 'accessing client id' do
  context 'before oauth2 file' do
    it 'raises an exception' do
      expect do
        class BeforeOauth2 < Soaspec::RestHandler
          base_url 'mandatory'
          headers 'client_id' => client_id
        end
      end.to raise_error RuntimeError, /Set by specifying credentials file/
    end
  end
  context 'after adding oauth2 file' do
    it 'reads correctly' do
      Soaspec.credentials_folder = 'credentials'
      class AfterOauth2 < Soaspec::RestHandler
        base_url 'mandatory'
        oauth2_file 'oauth2'
        headers client_id: client_id
      end
      expect(AfterOauth2.new.rest_client_headers[:client_id]).to eq 'THIS IS A TEST SECRET ID DSFJDSLKFJ'
    end
  end
end
