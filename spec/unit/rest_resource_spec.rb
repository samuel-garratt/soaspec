# frozen_string_literal: true

# This is to test the assumptions on testing the RestResource object as this used by RestHandler

describe RestClient::Resource do
  it 'handles query' do
    res = described_class.new('http://localhost:4999/echoer').get(params: { a: 1 })
    expect(res.request.url).to end_with '?a=1'
  end
end
