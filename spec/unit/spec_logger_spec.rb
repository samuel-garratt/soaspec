# frozen_string_literal: true

describe Soaspec::SpecLogger do
  it 'returns log file' do
    file = described_class.traffic_file
    expect(file).to be_instance_of String
    expect(file).to end_with '.log'
  end
  it 'can create a logger' do
    expect(described_class.create).to be_instance_of Soaspec::ApiLogger
  end
  it 'can have location changed' do
    described_class.traffic_folder = 'tmp_logs'
    expect(described_class.traffic_file).to start_with 'tmp_logs'
    described_class.traffic_folder = 'logs'
  end
  context 'view traffic' do
    it 'Only on terminal' do
      described_class.output_to_terminal = true
      expect(described_class.instance_variable_get(:@output_to_terminal)).to eq true
    end
    it 'Only on logs' do
      described_class.output_to_file = true
      expect(described_class.instance_variable_get(:@output_to_file)).to eq true
      described_class.output_to_file = false
    end
  end
  it 'has time format editable' do
    described_class.time_format = '%Y-%m-%d_%H:%M:%S'
    expect(described_class).to receive(:time_format).and_call_original
    described_class.info 'Date Test'
    described_class.time_format = nil # Unset
  end
  it 'has program name editable' do
    described_class.progname = 'Test Progname'
    described_class.info 'Prog Name Test'
    expect(described_class.progname).to eq 'Test Progname'
    expect(described_class).to receive :progname
    described_class.create
  end
  context 'info' do
    it 'handles array of messages' do
      described_class.create
      expect(described_class.logger).to receive(:info).exactly(2).times
      described_class.info %w[msg1 msg2]
    end
  end
end
