# frozen_string_literal: true

RSpec.describe Soaspec::TemplateReader do
  let(:test_file) { 'test_file.yml.erb' }
  let(:test_values) { { id: 'fhoehr328943' } }
  it 'can handle template_folder with nested directories' do
    Soaspec.template_folder = 'config/data'
    expect { subject.render_body(test_file, test_values) }.not_to raise_error
  end
  it 'can handle windows paths' do
    Soaspec.template_folder = 'config\\data'
    expect { subject.render_body(test_file, test_values) }.not_to raise_error
  end
  after do
    Soaspec.template_folder = 'templates'
  end
end
