# frozen_string_literal: true

class EchoService < Soaspec::RestHandler
  base_url 'http://localhost:4999/echoer'
end

RSpec.describe Soaspec::Wait, no_log: true do
  before(:each) { @time_before = Time.now.to_i }
  let(:failure_message) { 'custom message' }
  it 'waits for time specified' do
    expect do
      described_class.until(timeout: 3) { false }
    end.to raise_error Soaspec::TimeOutError
    time_after = Time.now.to_i
    expect(time_after - @time_before).to be_within(1).of 3
  end
  it 'does not fail if element not found' do
    described_class.until do
      call = EchoService.post(body: { id: '5' })
      element_to_check = Time.now.to_i > (@time_before + 3) ? :id : :not_there # Check correct thing after 3 seconds
      call[element_to_check] == '5'
    end
  end
  it 'can wait from an exchange itself' do
    time_before = @time_before # Needed to be available for execution within 'Exchange' instance
    EchoService.post(body: { id: '5' }).until do
      element_to_check = Time.now.to_i > (time_before + 3) ? :id : :not_there # Check correct thing after 3 seconds
      self[element_to_check] == '5'
    end
  end
  it 'returns itself' do
    exchange_after_waiting = EchoService.post(body: { id: '5', result: 'success' }).until do
      self[:id] == '5'
    end
    expect(exchange_after_waiting['result']).to eq 'success'
  end
  it 'can specify timeout' do
    expect do
      EchoService.post(body: { id: '5' }).until(timeout: 0.5) { self[:id] == '6' }
    end.to raise_error Soaspec::TimeOutError, /after 0.5 seconds/
  end
  it 'can specify interval' do
    expect do
      EchoService.post(body: { id: '5' }).until(timeout: 0.2, interval: 0.1) { self[:id] == '6' }
    end.to raise_error Soaspec::TimeOutError, /interval of 0.1/
  end
  it 'can specify message' do
    expect do
      EchoService.post(body: { id: '5' }).until(timeout: 0.4, message: failure_message) { self[:id] == '6' }
    end.to raise_error Soaspec::TimeOutError, /custom message/
  end
end
