# frozen_string_literal: true

require 'soaspec/wsdl_generator'

# @return [Hash] Simulating options method available to Thor
def options
  { string_default: 'test_string' }
end

describe Soaspec::WsdlGenerator do
  before(:all) do
    @wsdl_doc = Wasabi.document File.read('spec/unit/web_ratp.wsdl')
    @wsdl_schemas = @wsdl_doc.parser.schemas
    @content = "default:\n"
  end
  let(:first_operation_details) { @wsdl_doc.operations.values[0] }
  before(:each) { extend Soaspec::WsdlGenerator }
  describe '.value_after_namespace' do
    it('extracts value after :') { expect(value_after_namespace('test:element')).to eq 'element' }
    it('handles value without :') { expect(value_after_namespace('element')).to eq 'element' }
  end
  describe '.fill_in_field_from_type - handles xsd types' do
    it(String) { expect(fill_in_field_from_type('string')).to be_instance_of String }
    it(Integer) { expect(fill_in_field_from_type('int')).to be_instance_of(Integer).or be_instance_of(Fixnum) }
    it('Boolean') { expect(fill_in_field_from_type('boolean')).to be_instance_of TrueClass }
    it('Double') { expect(fill_in_field_from_type('double').to_f).to be_instance_of Float }
    context 'enumeration' do
      it 'parses custom schema' do
        expect(fill_in_field_from_type('ValuesType')).to eq "~randomize ['00001', '00002', '00003']"
      end
      it 'provides default value' do
        expect(fill_in_field_from_type('OtherType')).to eq 'Custom Type'
      end
    end
  end
  describe '.camel_case' do
    it('handles 1 word') { expect(camel_case('test')).to eq 'Test' }
    it('handles multiple words') { expect(camel_case('test_case_value')).to eq 'TestCaseValue' }
  end
  describe 'root element' do
    subject(:root) { root_element_for(first_operation_details) }
    it 'is a Nokogiri Element' do
      expect(root).to be_instance_of Nokogiri::XML::Element
    end
    it 'has correct name' do
      expect(root['name']).to eq 'getItineraries'
    end
  end
  describe 'root_elements' do
    let(:root_elements) { root_elements_for(first_operation_details) }
    it('is a nokogiri list') do
      expect(root_elements).to be_instance_of Nokogiri::XML::NodeSet
    end
    it 'has expected first element' do
      expect(root_elements.first['name']).to eq 'gpStart'
    end
    it('has expected size') { expect(root_elements.size).to eq 8 }
    it 'handles where type specified on root element' do
      @wsdl_doc = Wasabi.document File.read('spec/unit/bank.wsdl') # Read another wsdl other than default
      @wsdl_schemas = @wsdl_doc.parser.schemas
      operation = @wsdl_doc.operations.values[0]
      expect(root_element_for(operation)['type']).to eq 'tns:getBankType'
      expect(root_elements_for(operation).first['type']).to eq 'xsd:string'
    end
  end
  describe '.wsdl_to_yaml_for(list)' do
    it 'calculates types for multiple items in list' do
      wsdl_to_yaml_for root_elements_for(first_operation_details)
      expect(@content).to include 'gp_start: Custom Type'
    end
    it 'calculates types for where type set on root element' do
      @wsdl_doc = Wasabi.document File.read('spec/unit/bank.wsdl') # Read another wsdl other than default
      @wsdl_schemas = @wsdl_doc.parser.schemas
      operation = @wsdl_doc.operations.values[0]
      wsdl_to_yaml_for root_elements_for(operation)
      expect(@content).to include 'blz: test_string'
    end
    it 'calculates types for nested list' # Looks like Savon 3 template request is going to do this work for me. Not going to bother with this for now
  end
  describe 'generate_from_wsdl' do
    it 'creates required files' do
      expect(self).to receive(:create_file).at_least(7).times
      generate_from_wsdl(name: 'test', wsdl: 'spec/unit/web_ratp.wsdl')
    end
  end
end

describe '.document_type_for' do
  before(:all) do
    @wsdl_doc = Wasabi.document File.read('spec/unit/web_ratp.wsdl')
    @wsdl_schemas = @wsdl_doc.parser.schemas
    @content = "default:\n"
  end
  before(:each) { extend Soaspec::WsdlGenerator }
  let(:complex_xml) do
    '<xs:element name="getPerturbations">
        <xs:complexType>
          <xs:sequence>
            <xs:element minOccurs="0" name="perturbation" nillable="true" type="ns0:Perturbation"/>
            <xs:element minOccurs="0" name="isXmlText" type="xs:boolean"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>'
  end
  let(:documented_perturbation) { '  perturbation: Custom Type # Perturbation' }
  it 'handles complex type' do
    element = Nokogiri::XML(complex_xml)
    document_type_for(element.children[0], 0)
    expect(@content).to include documented_perturbation
    expect(@content).to include 'is_xml_text'
  end
end

describe 'Prompters' do
  before(:each) { extend Soaspec::WsdlGenerator }
  it '.ask_wsdl' do
    allow(STDIN).to receive(:gets) { 'test.wsdl' }
    ask_wsdl
    expect(@wsdl).to eq 'test.wsdl'
  end
  it '.name_of_wsdl' do
    allow(STDIN).to receive(:gets) { 'TestWsdl' }
    name_of_wsdl
    expect(@name).to eq 'TestWsdl'
  end
end
