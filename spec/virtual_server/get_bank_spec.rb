# frozen_string_literal: true

# Converts string to XML, throwing an error if anything cannot be parsed correctly
# @return [Nokogiri::XML::Document]
def return_xml_checking_syntax(xml_string)
  Nokogiri::XML(ERB.new(xml_string).result(binding)) do |config|
    config.options = Nokogiri::XML::ParseOptions::STRICT
  end
end

describe 'GetBank units' do
  before do
    @bank_name = 'test'
  end
  subject(:bank) { Soaspec::TestServer::GetBank }
  let(:soap_action) { 'get_bank' }
  it 'returns a wsdl' do
    expect do
      Nokogiri::XML(bank.test_wsdl) { |config| config.options = Nokogiri::XML::ParseOptions::STRICT }
    end.not_to raise_error
  end
  it 'returns success message' do
    @title = 'test val'
    xml = return_xml_checking_syntax bank.success_response_template
    expect(xml).to be_instance_of Nokogiri::XML::Document
    expect(xml.at_xpath('//ns1:bezeichnung', xml.collect_namespaces).inner_text).to eq @title
  end
  it 'handles bank not found' do
    xml = return_xml_checking_syntax bank.bank_not_found
    expect(xml).to be_instance_of Nokogiri::XML::Document
    expect(xml.at_xpath('//soapenv:Text', xml.collect_namespaces).inner_text).to end_with "#{@bank_name} gefunden!"
  end
  it 'handles incorrect request' do
    xml = return_xml_checking_syntax bank.error_response_template
    expect(xml).to be_instance_of Nokogiri::XML::Document
    expect(xml.at_xpath('//soapenv:Text', xml.collect_namespaces).inner_text).to end_with 'Unexpected subelement getBank'
  end
end

class MockRequest
  attr_accessor :env
  attr_accessor :body
end

describe 'GetBank.response_for' do
  let(:request) do
    request = MockRequest.new
    request.env = { 'HTTP_SOAPACTION' => 'getBank' }
    request
  end
  let(:success_body) do
    '<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://thomas-bayer.com/blz/" xmlns:env="http://www.w3.org/2003/05/soap-envelope">
  <env:Body>
    <tns:getBank>
      <tns:blz>550</tns:blz>
    </tns:getBank>
  </env:Body>
</env:Envelope>'
  end
  let(:fail_body) do
    '<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://thomas-bayer.com/blz/" xmlns:env="http://www.w3.org/2003/05/soap-envelope">
  <env:Body>
    <tns:getBank>
      <tns:blz>FAILURE</tns:blz>
    </tns:getBank>
  </env:Body>
</env:Envelope>'
  end
  subject(:bank) { Soaspec::TestServer::GetBank }
  it 'handles success' do
    request.body = success_body
    response = bank.response_for request
    expect(response).to be_instance_of String
    expect(response).to include 'Deutsche Bank'
    expect do
      return_xml_checking_syntax response
    end.not_to raise_error
  end
  it 'handles bank not found' do
    request.body = fail_body
    response = bank.response_for request
    expect(response).to be_instance_of Array
    expect(response[1]).to include 'Keine Bank'
    expect do
      return_xml_checking_syntax response[1]
    end.not_to raise_error
  end
end
