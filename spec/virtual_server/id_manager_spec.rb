# frozen_string_literal: true

require 'soaspec/virtual_server'

describe Soaspec::TestServer::IdManager do
  let(:general_case) { %w[1 1] }
  let(:failure_case) { %w[8 80] }
  context 'undeveloped' do
    before(:all) { Soaspec::TestServer::IdManager.developed = false }
    it 'returns true for general case' do
      ('1'..'13').each do |num|
        expect(described_class.result_for(num, '1')).to eq 'true'
      end
    end
    it 'returns false for failure case' do
      expect(described_class.result_for(*failure_case)).to eq 'false'
    end
  end
  context 'developed' do
    before(:all) { Soaspec::TestServer::IdManager.developed = true }
    it 'returns true for general case' do
      expect(described_class.result_for(*general_case)).to eq 'true'
    end
    it 'returns true for failure case' do
      expect(described_class.result_for(*failure_case)).to eq 'true'
    end
  end
end
