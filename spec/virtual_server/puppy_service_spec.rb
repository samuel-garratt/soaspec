# frozen_string_literal: true

require 'soaspec/test_server/puppy_service'

describe Soaspec::TestServer::PuppyService do
  it 'fresh id can be extracted each time' do
    described_class.new_id
    described_class.new_id
    expect(described_class.data.size).to eq 2
  end
end
