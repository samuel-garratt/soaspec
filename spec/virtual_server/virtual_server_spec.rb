# frozen_string_literal: true

require 'sinatra'
require 'rack'
require 'rack/test'
require 'rspec'

ENV['RACK_ENV'] = 'test'

require 'soaspec/virtual_server'

describe 'Virtual SOAP Service' do
  let(:browser) { Rack::Test::Session.new(Rack::MockSession.new(Soaspec::VirtualServer)) }
  it 'has wsdl' do
    browser.get '/BLZService'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include '<wsdl:documentation'
  end
  it 'redirects' do
    browser.get '/'
    expect(browser.last_response.status).to eq 302
  end
  it 'has text response' do
    browser.get '/text_response'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include 'ID='
  end
  it 'has server error' do
    browser.get '/server_error'
    expect(browser.last_response.body).to include 'Error'
    expect(browser.last_response.status).to eq 500
  end
  it 'has xml with attribute' do
    browser.get '/test_attribute'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include 'date='
  end
  it 'has namespace' do
    browser.get '/namespace'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include('h:table').and include('f:table')
  end
  it 'gets developed/undeveloped value' do
    browser.get '/packages/1/1'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include 'true'
  end
  it 'posts to make packages developed' do
    browser.post '/packages/developed', develop: 'true'
    expect(browser.last_response).to be_ok
  end
  it 'has a echo service that echos request body' do
    browser.post '/echoer', a: '1', b: '2'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to eq 'a=1&b=2'
  end
  it 'has echo service returning params sent' do
    browser.get '/echoer?a=1&b=2'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to eq '{"a"=>"1", "b"=>"2"}'
  end
  it 'returns multi layered path' do
    browser.get '/this/is/a/long/path'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include 'long path'
  end
  it 'has a oauth2 serivce simulator' do
    browser.post '/as/token.oauth2', username: 'test'
    expect(browser.last_response).to be_ok
    response_body = JSON.parse browser.last_response.body
    expect(response_body['access_token']).to start_with 'TEST_TOKEN'
    expect(response_body['token_type']).to start_with 'Bearer'
    expect(response_body).to have_key('expires_in').and have_key 'instance_url'
  end
  it 'returns HTTP authorization for debugging' do
    auth = 'Bearer access_token'
    browser.get '/invoice/1', {}, 'HTTP_AUTHORIZATION' => auth
    expect(browser.last_response).to be_ok
    response_body = JSON.parse browser.last_response.body
    expect(response_body['customer_id']).to eq '1'
    expect(response_body['oauth']).to eq auth
    expect(response_body['user']).to eq 'test'
  end
end

describe 'Baseline response' do
  let(:browser) { Rack::Test::Session.new(Rack::MockSession.new(Soaspec::VirtualServer)) }
  it 'responds for no params' do
    browser.get '/baseline'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include 'Empty'
  end
  it 'responds with suburls' do
    browser.get '/baseline/sub1/sub2'
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include 'sub1'
    expect(browser.last_response.body).to include 'sub2'
  end
end

describe 'Basic Auth' do
  let(:browser) { Rack::Test::Session.new(Rack::MockSession.new(Soaspec::VirtualServer)) }
  it 'demos protected data' do
    browser.get '/basic_secrets'
    expect(browser.last_response).not_to be_ok
  end
end
