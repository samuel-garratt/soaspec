# frozen_string_literal: true

require 'wasabi'
require 'savon'

# document = Savon.client(wsdl: 'test.wsdl').wsdl
# document = Savon.client(wsdl: 'http://www.webservicex.com/globalweather.asmx?wsdl').wsdl
# document = Wasabi.document File.read('test.wsdl')
document = Wasabi.document 'http://www.webservicex.net/ConvertTemperature.asmx?WSDL'
parser = document.parser

# TEST SCHEMA
#
# parser.schemas.each do |schema|
#   puts 'SCHEMA * '
#   puts schema
#
#   xsd = Nokogiri::XML::Schema(schema.to_s)
#
#   doc = Nokogiri::XML(File.read('test.xml'))
#
#   xsd.validate(doc).each do |error|
#     puts error.message
#   end
# end
#

schemes = parser.schemas
puts schemes
custom_type = schemes.xpath("//*[@name='TemperatureUnit']")
if custom_type.first
  puts 'CUST' + custom_type.to_s
  prefix = custom_type.first.namespace.prefix
  puts prefix

  enumerations = custom_type.xpath("//#{prefix}:enumeration")

  puts 'ENUM' + enumerations.to_s

  enumerations.each do |enum_value|
    puts enum_value['value']
  end
end
